from urllib2 import urlopen
import string

def preprocessPage(text):
    return string.replace(text, '/unimelb_engineers', 'https://www.bitbucket.org/unimelb_engineers')

def getPullRequestPage(url, id):
    # open the url for this pull request id, read it, and close it
    u = urlopen(url + str(id))
    text = u.read()
    u.close()
    # return the [processed] html read
    return preprocessPage(text)

def saveTextToFile(filename, text):
    f = open(filename, 'w')
    f.write(text)
    f.close()

def savePullRequestsBetween(url, start, finish, save_dir):
    # for each pull request in the given range (inclusive)
    for i in range(start, finish + 1):
        # make up a filename for it
        filename = save_dir + str(i) + '.html'
        # get its HTML
        text = getPullRequestPage(url, i)

        # save it to a file
        saveTextToFile(filename, text)

url = 'https://bitbucket.org/unimelb_engineers/swen-project-group-03-android/pull-request/'
save_dir = 'pull_requests/'

savePullRequestsBetween(url, 1, 45, save_dir)
