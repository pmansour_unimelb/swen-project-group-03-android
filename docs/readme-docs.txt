There are a lot of docs in here - there was a design doc to prepare for almost every big feature.

The most important docs to have a look at are:

-> InTouch - SRS
-> Usability Tests
-> Usability Testing Report

The following are a bit important for understanding the intuition behind our design:

-> Data Models
-> Emergency Button design 2.0

The following contains some useful scenarios you might want to try on the system:
-> Scenarios to run

Design docs used to describe features before diving into code:

-> Notifications

Old (but not necessarily outdated):

-> Process

Outdated, but kept for historical reasons:

-> Architectural Design
-> Coding Standards
-> Last use time (was changed drastically)