docs/ - contains all our docs, and its own readme file
pull_requests/ - contains all our pull requests in html, and a python script that can download them all again

Instructions
~~~~~~~~~~~~

Creating the projects in eclipse:

1. Clone the repo into a folder on your system (let's call it "team03")
2. Open Eclipse
3. File->New->Other
4. Under "Android", choose "Android Project from Existing Code"
5. Under "Root Directory", browse to the repository folder (team03)
6. Choose both the "app" and the "InTouchTestsTest" projects
7. Click "Finish" at the bottom

Running the app:

1. Connect your device to the computer (or start GenyMotion or your Android Emulator)
2. Right-click on the InTouch project in Eclipse
3. Choose "Run As", then choose "Android Project"

Running the tests:

1. Connect your device to the computer (or start GenyMotion or your Android Emulator)
2. Right-click on the InTouchTestsTest project in Eclipse
3. Choose "Run As", then choose "Android JUnit Test"

Unimplemented things
~~~~~~~~~~~~~~~~~~~~

* Yellow and Green notifications sent to carers
* Attach photos and videos to messages
* Change profile photo