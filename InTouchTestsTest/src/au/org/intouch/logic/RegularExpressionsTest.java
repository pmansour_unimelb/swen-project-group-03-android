package au.org.intouch.logic;

import org.junit.Test;

import android.test.AndroidTestCase;

// test class for the regular expression class
public class RegularExpressionsTest extends AndroidTestCase{
	
	private static String email1 = "notAnEmail";
	private static String email2 = "not@nEmailEither";
	private static String email3 = "acceptable@email.com";
	private static String email4 = "un@acceptable@email.com";
	private static String email5 = "ACceptAble@EMAIL.com";

	private static String phone1 = "12345";
	private static String phone2 = "abcdef";
	private static String phone3 = "12345678901234567890";
	private static String phone4 = "123456789012345678901";
	private static String phone5 = "+12345";
	private static String phone6 = "+1234";
	private static String phone7 = "0425549000";
	
	@Test
	public void testEmail1() {
		assertEquals(Validator.isEmail(email1), false);
	}
	
	@Test
	public void testEmail2() {
		assertEquals(Validator.isEmail(email2), false);
	}
	
	@Test
	public void testEmail3() {
		assertEquals(Validator.isEmail(email3), true);
	}
	
	@Test
	public void testEmail4() {
		assertEquals(Validator.isEmail(email4), false);
	}
	
	@Test
	public void testEmail5() {
		assertEquals(Validator.isEmail(email5), true);
	}
	
	
	// phone tests
	@Test
	public void testPhone1(){
		assertEquals(Validator.isPhone(phone1), true);
	}
	
	@Test
	public void testPhone2(){
		assertEquals(Validator.isPhone(phone2), false);
	}
	
	@Test
	public void testPhone3(){
		assertEquals(Validator.isPhone(phone3), true);
	}
	
	@Test
	public void testPhone4(){
		assertEquals(Validator.isPhone(phone4), false);
	}
	
	@Test
	public void testPhone5(){
		assertEquals(Validator.isPhone(phone5), true);
	}
	
	@Test
	public void testPhone6(){
		assertEquals(Validator.isPhone(phone6), false);
	}
	@Test
	public void testPhone7(){
		assertEquals(Validator.isPhone(phone7), true);
	}

}
