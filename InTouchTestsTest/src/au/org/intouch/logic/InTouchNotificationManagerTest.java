package au.org.intouch.logic;

import java.util.Date;

import android.test.AndroidTestCase;
import au.org.intouch.models.AssistedPerson;
import au.org.intouch.models.Notification;
import au.org.intouch.models.UnassistedPerson;
import au.org.intouch.models.Notification.NotificationType;

public class InTouchNotificationManagerTest extends AndroidTestCase {

	private InTouchNotificationManager _nm;
	
	@Override
	public void setUp() {
		_nm = new InTouchNotificationManager(getContext());
	}
	@Override
	public void tearDown() {
		_nm = null;
	}
	
	// Fake stuff
	public AssistedPerson fakeAP() {
		AssistedPerson ap;
		
		ap = new AssistedPerson("person1");
		ap.setFirstName("Tony");
		ap.setLastName("AfterAnAccident");
		ap.setPhoneNumber("0458149243");
		
		return ap;
	}
	public UnassistedPerson fakeUP() {
		UnassistedPerson up;
		
		up = new UnassistedPerson("person2");
		up.setFirstName("Jimmy");
		up.setFirstName("TheKid");
		up.setPhoneNumber("0425549000");
		
		return up;
	}
	public Notification fakeNotification(NotificationType type) {
		Notification n;
		
		n = new Notification("notification");
		n.setRead(false);
		n.setSender(fakeAP());
		n.setRecipient(fakeUP());
		n.setTime(new Date());
		n.setType(type);
		
		return n;
	}
	
	// currently, this always crashes since dialogs need an activity context
	// not an application context, but all we can offer in this type of test
	// case is an application context
	public void testRedDoesntCrash() {
		_nm.showNotification(fakeNotification(NotificationType.RED));
	}
	public void testYellowDoesntCrash() {
		_nm.showNotification(fakeNotification(NotificationType.YELLOW));
	}
	public void testGreenDoesntCrash() {
		_nm.showNotification(fakeNotification(NotificationType.GREEN));
	}
}
