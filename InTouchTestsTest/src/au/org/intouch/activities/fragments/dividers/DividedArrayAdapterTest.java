package au.org.intouch.activities.fragments.dividers;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.test.AndroidTestCase;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import au.org.intouch.R;

public class DividedArrayAdapterTest extends AndroidTestCase {

	private static enum Type { Short, Long };
	private static class StringLengthDividedAdapter extends DividedArrayAdapter<String, Type> {

		public StringLengthDividedAdapter(Context context, List<String> objects) {
			super(context, R.layout.contact_list_item, objects);
		}
		@Override
		protected Type getType(String object) {
			return object.length() < 5 ? Type.Short : Type.Long;
		}
		@Override
		protected View getDividerView(Type type, View convertView,
				ViewGroup parent) {
			TextView dividerView;
			
			dividerView = new TextView(getContext());
			
			dividerView.setText("Divider: " + type.toString());
			
			return dividerView;
		}
		@Override
		protected View getValueView(String value, View convertView,
				ViewGroup parent) {
			TextView itemView;
			
			itemView = new TextView(getContext());
			
			itemView.setText("Item: " + value);
			
			return itemView;
		}
		
		
	}
	
	private List<String> _objects;
	private StringLengthDividedAdapter _adapter;
	
	@Override
	public void setUp() {
		// create a list of strings to test
		_objects = new ArrayList<String>();
		_objects.add("longest");
		_objects.add("sh");
		_objects.add("SH");
		_objects.add("longer");
		_objects.add("not Short");
		_objects.add("");
		
		// create an adapter for them
		_adapter = new StringLengthDividedAdapter(getContext(), _objects);
		
	}
	
	
	public void testListItems() {		
		// first should be a divider for Short
		assertTrue(_adapter.getItem(0).isDivider());
		assertEquals(_adapter.getItem(0).getTypeValue(), Type.Short);
		
		// followed by some short strings
		assertTrue(!_adapter.getItem(1).isDivider());
		assertEquals(_adapter.getItem(1).getItemValue(), "sh");
		
		assertTrue(!_adapter.getItem(2).isDivider());
		assertEquals(_adapter.getItem(2).getItemValue(), "SH");
		
		assertTrue(!_adapter.getItem(3).isDivider());
		assertEquals(_adapter.getItem(3).getItemValue(), "");
		
		// then a divider for long
		assertTrue(_adapter.getItem(4).isDivider());
		assertEquals(_adapter.getItem(4).getTypeValue(), Type.Long);
		
		// followed by the long strings
		assertTrue(!_adapter.getItem(5).isDivider());
		assertEquals(_adapter.getItem(5).getItemValue(), "longest");
		
		assertTrue(!_adapter.getItem(6).isDivider());
		assertEquals(_adapter.getItem(6).getItemValue(), "longer");
		
		assertTrue(!_adapter.getItem(7).isDivider());
		assertEquals(_adapter.getItem(7).getItemValue(), "not Short");
	}
	
	public void testDividerViews() {
		View view;
		
		// for each divider
		for(int i = 0; i < _adapter.getCount(); i++) {
			if(_adapter.getItem(i).isDivider()) {
				// get its view
				view = _adapter.getView(i, null, null);
				
				// make sure it's a text view
				assertTrue(view instanceof TextView);
				
				// make sure it's got the correct text
				assertEquals(
						((TextView) view).getText().toString(),
						"Divider: " + _adapter.getItem(i).getTypeValue().toString()
					);
			}
		}
	}
	
	public void testItemViews() {
		View view;
		
		// for each item
		for(int i = 0; i < _adapter.getCount(); i++) {
			if(!_adapter.getItem(i).isDivider()) {
				// get its view
				view = _adapter.getView(i, null, null);
				
				// make sure it's a text view
				assertTrue(view instanceof TextView);
				
				// make sure it's got the correct text
				assertEquals(
						((TextView) view).getText().toString(),
						"Item: " + _adapter.getItem(i).getItemValue()
					);
			}
		}
	}
}
