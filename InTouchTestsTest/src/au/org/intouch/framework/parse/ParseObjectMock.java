package au.org.intouch.framework.parse;

import com.parse.ParseObject;

/**
 * A class that mocks out the save methods of ParseObjects.
 * @author peter
 *
 */
public class ParseObjectMock extends ParseObject {
	
	public ParseObjectMock(String className) {
		super(className);
	}

}
