package au.org.intouch.framework.parse;

import java.util.Date;

import com.parse.Parse;
import com.parse.ParseObject;
import com.parse.ParseUser;

import android.test.AndroidTestCase;
import au.org.intouch.InTouch;
import au.org.intouch.models.AssistedPerson;
import au.org.intouch.models.ContactRequest;
import au.org.intouch.models.Message;
import au.org.intouch.models.Notification;
import au.org.intouch.models.Notification.NotificationType;
import au.org.intouch.models.Relation;
import au.org.intouch.models.Role;
import au.org.intouch.models.UnassistedPerson;

public class ParseEntityAdapterTest extends AndroidTestCase {

	public ParseEntityAdapterTest() {
		super();
	}
	
	@Override
	public void setUp() {		
		// set up parse
        Parse.initialize(getContext(), InTouch.PARSE_APPLICATION_ID, InTouch.PARSE_CLIENT_KEY);
	}
	@Override
	public void tearDown() {
		
	}
	
	// make up some fake entities
	private static AssistedPerson fakeAP() {
		AssistedPerson ap;
		
		ap = new AssistedPerson("lcadence");
		ap.setFirstName("Lavone");
		ap.setLastName("Cadence");
		ap.setEmail("lcadence@gmail.com");
		ap.setPhoneNumber("+61411111111");
		
		ap.setLastCommunicationTime(new Date(0));
		ap.setLastInteractionTime(new Date(0));
		
		return ap;
	}
	private static ParseUser fakeParseAP() {
		ParseUser ap;
		
		// create the parse user
		ap = ParseUser.create(ParseUser.class);
		// give them the object id we want
		ap.setObjectId("lcadence");

		// set some of their fields
		ap.setUsername("lcadence@gmail.com");
		ap.setEmail("lcadence@gmail.com");
		ap.setPassword("password");

		ap.put(ParseEntityAdapter.User_Assisted, true);

		ap.put(ParseEntityAdapter.User_FirstName, "Lavone");
		ap.put(ParseEntityAdapter.User_LastName, "Cadence");
		ap.put(ParseEntityAdapter.User_Email, "lcadence@gmail.com");
		ap.put(ParseEntityAdapter.User_PhoneNumber, "+61411111111");
		ap.put(ParseEntityAdapter.User_LCT, new Date(0));
		ap.put(ParseEntityAdapter.User_LIT, new Date(0));


		// return that user
		return ap;
	}
	private static UnassistedPerson fakeUP() {
		UnassistedPerson up;
		
		up = new UnassistedPerson("tstark");
		up.setFirstName("Tony");
		up.setLastName("Stark");
		up.setEmail("tstark@gmail.com");
		up.setPhoneNumber("+61400000000");
		
		return up;
	}
	private static ParseUser fakeParseUP() {
		ParseUser up;

		// create the parse user
		up = ParseUser.create(ParseUser.class);
		// give them the object id we want
		up.setObjectId("tstark");

		// set some of their fields
		up.setUsername("tstark@gmail.com");
		up.setEmail("tstark@gmail.com");
		up.setPassword("password");

		up.put(ParseEntityAdapter.User_Assisted, false);

		up.put(ParseEntityAdapter.User_FirstName, "Tony");
		up.put(ParseEntityAdapter.User_LastName, "Stark");
		up.put(ParseEntityAdapter.User_Email, "tstark@gmail.com");
		up.put(ParseEntityAdapter.User_PhoneNumber, "+61400000000");

		// return that user
		return up;
	}
	private static Relation fakeRelation() {
		Relation rel;
		
		rel = new Relation("starkHelpsLavone");
		rel.setAssistedPerson(fakeAP());
		rel.setUnassistedPerson(fakeUP());
		rel.setRole(Role.PrimaryCarer);
		
		return rel;
	}
	private static ParseObject fakeParseRelation() {
		ParseObject o;
		
		// create the parse object
		o = ParseObject.create(ParseEntityAdapter.Relation_ClassName);

		o.setObjectId("starkHelpsLavone");

		o.put(ParseEntityAdapter.Relation_AssistedPerson, fakeParseAP());
		o.put(ParseEntityAdapter.Relation_UnassistedPerson, fakeParseUP());
		o.put(ParseEntityAdapter.Relation_Role, Role.PrimaryCarer.toString());

		return o;
	}
	private static ContactRequest fakeAssistanceOffer() {
		ContactRequest cr;
		
		cr = new ContactRequest("stark2lavone");
		cr.setInitiator(fakeUP());
		cr.setRecipientEmail(fakeAP().getEmail());
		cr.setDesiredRole(Role.PrimaryCarer);
		
		return cr;
	}
	private static ParseObject fakeParseAssistanceOffer() {
		ParseObject o;
		
		// create the parse object
		o = ParseObject.create(ParseEntityAdapter.ContactRequest_ClassName);

		o.setObjectId("stark2lavone");

		o.put(ParseEntityAdapter.ContactRequest_Type, ParseEntityAdapter.ContactRequest_Type_Offer);

		o.put(ParseEntityAdapter.ContactRequest_Initiator, fakeParseUP());
		o.put(ParseEntityAdapter.ContactRequest_RecipientEmail, fakeAP().getEmail());
		o.put(ParseEntityAdapter.ContactRequest_DesiredRole, Role.PrimaryCarer.toString());
		
		return o;
	}
	private static ContactRequest fakeAssistanceRequest() {
		ContactRequest cr;
		
		cr = new ContactRequest("lavone2stark");
		cr.setInitiator(fakeAP());
		cr.setRecipientEmail(fakeUP().getEmail());
		cr.setDesiredRole(Role.PrimaryCarer);
		
		return cr;
	}
	private static ParseObject fakeParseAssistanceRequest() {
		ParseObject o;
		
		// create the parse object
		o = ParseObject.create(ParseEntityAdapter.ContactRequest_ClassName);

		o.setObjectId("lavone2stark");

		o.put(ParseEntityAdapter.ContactRequest_Type, ParseEntityAdapter.ContactRequest_Type_Request);

		o.put(ParseEntityAdapter.ContactRequest_Initiator, fakeParseAP());
		o.put(ParseEntityAdapter.ContactRequest_RecipientEmail, fakeUP().getEmail());
		o.put(ParseEntityAdapter.ContactRequest_DesiredRole, Role.PrimaryCarer.toString());
		
		return o;
	}
	private static Message fakeMessage() {
		Message m;
		
		m = new Message("evenparse");
		m.setSender(fakeAP());
		m.setRecipient(fakeUP());
		m.setTime(new Date(1));
		m.setRead(false);
		
		m.setBody("Do you even parse?");
		
		return m;
	}
	private static ParseObject fakeParseMessage() {
		ParseObject o;
		
		// create the parse object
		o = ParseObject.create(ParseEntityAdapter.Communication_ClassName);

		o.setObjectId("evenparse");

		o.put(ParseEntityAdapter.Communication_Type, ParseEntityAdapter.Communication_Type_Message);

		o.put(ParseEntityAdapter.Communication_Sender, fakeParseAP());
		o.put(ParseEntityAdapter.Communication_Recipient, fakeParseUP());
		o.put(ParseEntityAdapter.Communication_Time, new Date(1));
		o.put(ParseEntityAdapter.Communication_Read, false);
		o.put(ParseEntityAdapter.Communication_Message_Body, "Do you even parse?");
		
		return o;
	}
	private static Notification fakeNotification() {
		Notification n;
		
		n = new Notification("redn");
		n.setSender(fakeAP());
		n.setRecipient(fakeUP());
		n.setTime(new Date(2));
		n.setRead(false);
		
		n.setType(NotificationType.RED);
		
		return n;
	}
	private static ParseObject fakeParseNotification() {
		ParseObject o;
		
		// create the parse object
		o = ParseObject.create(ParseEntityAdapter.Communication_ClassName);

		o.setObjectId("redn");

		o.put(ParseEntityAdapter.Communication_Type, ParseEntityAdapter.Communication_Type_Notification);

		o.put(ParseEntityAdapter.Communication_Sender, fakeParseAP());
		o.put(ParseEntityAdapter.Communication_Recipient, fakeParseUP());
		o.put(ParseEntityAdapter.Communication_Time, new Date(2));
		o.put(ParseEntityAdapter.Communication_Read, false);
		o.put(ParseEntityAdapter.Communication_Notification_Type, NotificationType.RED.toString());
		
		return o;
	}
	
	// test toParseObject
	
	public void testToParseObject_Communication_Message() {
		ParseObject message, messageUnderTest;
		
		// get what the message should be like
		message = fakeParseMessage();
		
		// test the conversion method
		messageUnderTest = ParseEntityAdapter.toParseObject(fakeMessage());
		
		// make sure all the fields are what they should be
		assertEquals(messageUnderTest.getObjectId(), message.getObjectId());
		assertEquals(
					messageUnderTest.get(ParseEntityAdapter.Communication_Type),
					message.get(ParseEntityAdapter.Communication_Type)
				);
		assertEquals(
					messageUnderTest.getParseObject(ParseEntityAdapter.Communication_Sender).getObjectId(),
					message.getParseObject(ParseEntityAdapter.Communication_Sender).getObjectId()
				);
		assertEquals(
					messageUnderTest.getParseObject(ParseEntityAdapter.Communication_Recipient).getObjectId(),
					message.getParseObject(ParseEntityAdapter.Communication_Recipient).getObjectId()
				);
		assertEquals(
					messageUnderTest.get(ParseEntityAdapter.Communication_Time),
					message.get(ParseEntityAdapter.Communication_Time)
				);
		assertEquals(
					messageUnderTest.get(ParseEntityAdapter.Communication_Read),
					message.get(ParseEntityAdapter.Communication_Read)
				);
		assertEquals(
					messageUnderTest.get(ParseEntityAdapter.Communication_Message_Body),
					message.get(ParseEntityAdapter.Communication_Message_Body)
				);
	}
	public void testToParseObject_Communication_Notification() {
		Notification fakeNotification;
		ParseObject pNotification;
		
		// get the fake notification
		fakeNotification = fakeNotification();
		
		// test the conversion method
		pNotification = ParseEntityAdapter.toParseObject(fakeNotification);
		
		// make sure all the fields are correct
		assertEquals(pNotification.getObjectId(), fakeNotification.getId());
		assertEquals(pNotification.get(ParseEntityAdapter.Communication_Type), ParseEntityAdapter.Communication_Type_Notification);
		assertEquals(
					pNotification.getParseObject(ParseEntityAdapter.Communication_Sender).getObjectId(),
					fakeNotification.getSender().getId()
				);
		assertEquals(
					pNotification.getParseObject(ParseEntityAdapter.Communication_Recipient).getObjectId(),
					fakeNotification.getRecipient().getId()
				);
		assertEquals(pNotification.get(ParseEntityAdapter.Communication_Time), fakeNotification.getTime());
		assertEquals(pNotification.get(ParseEntityAdapter.Communication_Read), fakeNotification.getRead());
		assertEquals(pNotification.get(ParseEntityAdapter.Communication_Notification_Type), fakeNotification.getType().toString());
	}
	public void testToParseObject_ContactRequest() {
		ContactRequest fakeCR;
		ParseObject pCR;
		
		// get the fake contact request
		fakeCR = fakeAssistanceOffer();
		
		// test the conversion method
		pCR = ParseEntityAdapter.toParseObject(fakeCR);
		
		// make sure all the fields are correct
		assertEquals(pCR.getObjectId(), fakeCR.getId());
		assertEquals(pCR.get(ParseEntityAdapter.ContactRequest_Type), ParseEntityAdapter.ContactRequest_Type_Offer);
		assertEquals(
					pCR.getParseObject(ParseEntityAdapter.ContactRequest_Initiator).getObjectId(),
					fakeCR.getInitiator().getId()
				);
		assertEquals(pCR.get(ParseEntityAdapter.ContactRequest_RecipientEmail), fakeCR.getRecipientEmail());
		assertEquals(pCR.get(ParseEntityAdapter.ContactRequest_DesiredRole), fakeCR.getDesiredRole().toString());
	}
	public void testToParseObject_Relation() {
		Relation fakeRel;
		ParseObject pRel;
		
		// get the fake relation
		fakeRel = fakeRelation();
		
		// test the conversion method
		pRel = ParseEntityAdapter.toParseObject(fakeRel);
		
		// make sure all the fields are correct
		assertEquals(pRel.getObjectId(), fakeRel.getId());
		assertEquals(
					pRel.getParseObject(ParseEntityAdapter.Relation_AssistedPerson).getObjectId(),
					fakeRel.getAssistedPerson().getId()
				);
		assertEquals(
					pRel.getParseObject(ParseEntityAdapter.Relation_UnassistedPerson).getObjectId(),
					fakeRel.getUnassistedPerson().getId()
				);
		assertEquals(pRel.get(ParseEntityAdapter.Relation_Role), fakeRel.getRole().toString());
	}
	public void testToParseObject_User() {
		AssistedPerson fakeUser;
		ParseObject pUser;
		
		// get the fake assisted person
		fakeUser = fakeAP();
		
		// test the conversion method
		pUser = ParseEntityAdapter.toParseObject(fakeUser);
		
		// make sure the class type and id are correct
		assertEquals(pUser.getObjectId(), fakeUser.getId());
		
		// make sure all the fields are correct
		assertEquals(pUser.get(ParseEntityAdapter.User_Assisted), true);
		assertEquals(pUser.get(ParseEntityAdapter.User_FirstName), fakeUser.getFirstName());
		assertEquals(pUser.get(ParseEntityAdapter.User_LastName), fakeUser.getLastName());
		assertEquals(pUser.get(ParseEntityAdapter.User_Email), fakeUser.getEmail());
		assertEquals(pUser.get(ParseEntityAdapter.User_PhoneNumber), fakeUser.getPhoneNumber());
		assertEquals(pUser.get(ParseEntityAdapter.User_LCT), fakeUser.getLastCommunicationTime());
		assertEquals(pUser.get(ParseEntityAdapter.User_LIT), fakeUser.getLastInteractionTime());
	}
	
	// test fromParseObject
	
	public void testFromParseObject_ContactRequest_Offer() {
		ContactRequest cr;
		
		// convert the fake parse assistance offer to a CR object
		cr = ParseEntityAdapter.fromParseObject(fakeParseAssistanceOffer(), ContactRequest.class);
		
		// make sure it's the same as the fake assistance offer
		assertTrue(cr.equals(fakeAssistanceOffer()));
	}
	public void testFromParseObject_ContactRequest_Request() {
		ContactRequest cr;
		
		// convert the fake parse assistance request to a CR object
		cr = ParseEntityAdapter.fromParseObject(fakeParseAssistanceRequest(), ContactRequest.class);
		
		// make sure it's the same as the fake assistance request
		assertTrue(cr.equals(fakeAssistanceRequest()));
	}
	public void testFromParseObject_Communication_Message() {
		Message message;
		
		// convert the fake parse message to a Message object
		message = ParseEntityAdapter.fromParseObject(fakeParseMessage(), Message.class);
		
		// make sure it's the same as the fake message object
		assertTrue(message.equals(fakeMessage()));
	}
	public void testFromParseObject_Communication_Notification() {
		Notification notification;
		
		// convert the fake parse notification to a Notification object
		notification = ParseEntityAdapter.fromParseObject(fakeParseNotification(), Notification.class);
		
		// make sure it's the same as the fake notification object
		assertTrue(notification.equals(fakeNotification()));
	}
	public void testFromParseObject_Relation() {
		Relation rel;
		
		// convert the fake parse relation to a Relation object
		rel = ParseEntityAdapter.fromParseObject(fakeParseRelation(), Relation.class);
		
		// make sure it's the same as the fake relation object
		assertTrue(rel.equals(fakeRelation()));
	}
	public void testFromParseObject_User_AP() {
		AssistedPerson u;
		
		// convert the fake parse assisted person to an AssistedPerson object
		u = ParseEntityAdapter.fromParseObject(fakeParseAP(), AssistedPerson.class);
		
		// make sure it's the same as the fake AP object
		assertTrue(u.equals(fakeAP()));
	}
	public void testFromParseObject_User_UP() {
		UnassistedPerson u;
		
		// convert the fake parse assisted person to an UnassistedPerson object
		u = ParseEntityAdapter.fromParseObject(fakeParseUP(), UnassistedPerson.class);
		
		// make sure it's the same as the fake UP object
		assertTrue(u.equals(fakeUP()));
	}
}