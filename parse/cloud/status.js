Parse.Cloud.beforeSave('Status', function (req, response) {
  var field
    , user = req.user
    , time = req.object.get('time');

  req.object.set('user', user);

  if (req.object.get('emergency')) {
    // TODO: send an alert to all carers
  }

  var type = req.object.get('type');

  var query = new Parse.Query(Parse.User);
  query.equalTo('objectId', req.user.id);
  query.find({
    success: function(users) {
      var u = users[0];

      if (type === 'contact') {
        u.set('lastCommunicationTime', time);

      } else if (type === 'used') {
        u.set('lastDeviceUsedTime', time);
      }

      u.save();
      response.success();
    },
    error: function(error) {
      response.error("Error finding user " + error.code + ": " + error.message);
    }
  });



});