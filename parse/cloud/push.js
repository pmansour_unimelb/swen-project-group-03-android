module.exports = Push;

function Push(data) {
  this.data = data;
}

Push.prototype.sendToEmail = function (email, response) {
  var userQuery = new Parse.Query(Parse.User);
  userQuery.equalTo('email', email);

  var pushQuery = new Parse.Query(Parse.Installation);
  pushQuery.matchesQuery('owner', userQuery);
   
  Parse.Push.send({
    where: pushQuery, // Set our Installation query
    data: this.data
  }, response);
};

Push.prototype.sendToUser = function (user, response) {
  var pushQuery = new Parse.Query(Parse.Installation);
  pushQuery.equalTo('owner', user.id);

  Parse.Push.send({
    where: pushQuery, // Set our Installation query
    data: this.data
  }, response);
};