var Email   = require('cloud/email')
  , Push    = require('cloud/push');

function noop() { }
noop.success = 
noop.error   = function (result) {
  console.log('errror!!!!!!');
  console.log(result);
  // console.log(result);
};

function getMessage (initiator, recipient) {
  var fullName = initiator.get('firstName') + ' ' + initiator.get('lastName');
  return fullName + ' wants to keep in touch using InTouch';
}

var Role = {
  primary: 'Primary Carer',
  carer: 'Carer',
  friend: 'Friend'
};

function deliver (response) {

  var role = this.get('role');
  var email = this.get('email');

  var contactRequest = this;

  this.get('initiator').fetch({
    success: function(initiator) {
      findUserByEmail(email).find({
        success: function (recipients) {
          var recipient = recipients.pop();


          // if the recipient has already signed up...
          if (recipient) {

            // then for these types of roles, enforce that only one may be assisted
            if (true || (role === Role.primary || role === Role.carer)) {
              if (recipient.get('assisted') && initiator.get('assisted')) {
                return response.error('Both are assisted!');
              } else if (!recipient.get('assisted') && !initiator.get('assisted')) {
                return response.error('None are assisted!' + recipient.get('assisted') + initiator.get('assisted'));
              }
            }
          }


          var title = getMessage.call(contactRequest, initiator, recipient);


          var push = new Push({
            alert: title
          });

          var emailMessage = new Email({
            from: "intouch.android.app@gmail.com",
            subject: title,
            text: "Accept their contact request using the InTouch app on your Android phone."
          });

          push.sendToEmail(email, noop);


          emailMessage.sendToEmail(email, noop);

          response.success();

        },
        error: function (err) {
          response.error(err);
        }
      })
    }
  });

  


};

function sendMessage(from, to, response) {
  var Communication = Parse.Object.extend('Communication');
  var message = new Communication();

  message.set('type', 'message');
  message.set('body');
  message.set('sender', from);
  message.set('recipient', to);
  message.set('time', new Date());
  message.set('read', false);
  message.set('notificationType', 'none');

  var push = new Push({
    alert: 'Accepted'
  });

  push.sendToUser(to, noop);

  message.save(null, response);

}

function findUserByEmail(email) {

  var query = new Parse.Query(Parse.User);
  query.equalTo('email', email);
  return query;
}


var AP = 'RelationAssistedPerson';
var UAP = 'RelationUnassistedPerson';
var ROLE = 'RelationRole';


function downgradeAllPrimaryCarers(assisted, response) {
  console.log('downgrade other primary carers...');
  if (!assisted) {
    console.log('no assisted person!');
    return response.success();
  }

  var query = new Parse.Query('Relation');

  query.equalTo(AP, assisted.id);
  query.equalTo(ROLE, Role.primary);

  console.log('query...');
  query.find({
    success: function (results) {
      console.log('success');
      var result = results.pop();

      if (!result) {
        console.log('nothing to delete');
        response.success();
        return;
      }
      console.log('deleting other primary carer...');
      result.set(ROLE, Role.carer);
      console.log('save...');
      result.save(null, {
        success: function () {
          console.log('saved');
          response.success();
        },
        error: function (err) {
          console.log('error 3');
          response.error(err);
        }
      });
    },
    error: function (err) {
      console.log('error 2');
      console.log(err);
      response.error(err);
    }
  });

}

function accept (response) {

  var recipientEmail = this.get('email');
  var role = this.get('role');


  this.get('initiator').fetch({
    success: function(initiator) {
      

      var ap;
      if (initiator.get('assisted')) {
        ap = initiator;
      }

      findUserByEmail(recipientEmail).find({
        success: function (recipients) {
          console.log('users with email: ' + recipientEmail + ': ' + recipients.length);

          var recipient = recipients.pop();


          var Relation = Parse.Object.extend('Relation');

          function after() {
            console.log('query generate');

            var query = new Parse.Query('Relation');

            if (initiator.get('assisted')) {
              // if the assisted person was the initiator
              query.equalTo(AP, initiator.id);
              query.matchesQuery(UAP, findUserByEmail(recipientEmail));
            } else {
              // if the unassisted person was the initiator
              query.equalTo(UAP, initiator.id);
              query.matchesQuery(AP, findUserByEmail(recipientEmail));
            }

            console.log('query find');
            query.find({
              success: function(relations) {
                console.log('query success');

                var relation = relations.pop();

                relation = relation || new Relation();

                relation.set(ROLE, role);

                var recipientField;

                if (initiator.get('assisted')) {
                  relation.set(AP, initiator);
                  recipientField = UAP;
                } else {
                  relation.set(UAP, initiator);
                  recipientField = AP;
                }

                relation.set(recipientField, recipient);
                console.log('save relation');
                relation.save(null, {
                  success: function () {

                    console.log('save success');
                    sendMessage(recipient, initiator, noop);
                    response.success();
                  },
                  error: function () {
                    response.error('Could not save relation');
                  }
                });
              },
              error: function (message) {
                response.error(message);
              }
            });
          }

          if (role === Role.primary) {
            downgradeAllPrimaryCarers(ap || recipient, {
              success: function () {
                console.log('downgraded success');
                after();
              },
              error: function () {
                // yeah, whatever
                console.log('downgraded failure');
                after();
              }
            });
          } else {
            console.log('other than primary');
            after();
          }

        },
        error: function(err) {
          console.log('lookup err:');
          console.log(err);
          response.error("lookup failed");
        }
      });
    }
  });



}

Parse.Cloud.beforeSave('ContactRequest', function(request, response) {

  var object = request.object;

  if (request.object.isNew()) {
    deliver.call(object, response);
  } else if (object.get('accepted')) {
    accept.call(object, response);
  } else {
    response.success();
  }

});
