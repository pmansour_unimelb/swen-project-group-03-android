package au.org.intouch.logic.interfaces;

import java.io.Serializable;

public interface DeviceFunctionality extends Serializable {
	// alternate vibratations 5 times a second
	public static final long[] VIBRATE_ALTERNATING =
		{ 0L, 200L, 200L, 200L, 200L, 200L };

	public void call(String number);
	
	public void vibrate(long milliseconds);
	public void vibrate(long[] pattern, int repeatFrom);
	public void stopVibrating();
}
