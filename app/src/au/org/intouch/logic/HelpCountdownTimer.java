package au.org.intouch.logic;

import java.util.List;

import android.app.Activity;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import au.org.intouch.InTouch;
import au.org.intouch.framework.exceptions.InTouchException;
import au.org.intouch.framework.interfaces.PassiveCallback;
import au.org.intouch.framework.interfaces.ResultCallback;
import au.org.intouch.logic.interfaces.DeviceFunctionality;
import au.org.intouch.models.UnassistedPerson;
import au.org.intouch.models.Notification.NotificationType;

/**
 * A class that counts down to a certain time window, updating
 * a given TextView in the process. After the window is finished,
 * it uses the given DeviceFunctionality object to call a given
 * User object, and shuts down the starting Activity.
 * 
 * @author peter
 *
 */
public class HelpCountdownTimer extends CountDownTimer {
	// how often to update the text view
	private static final long TICK_FREQUENCY = 1000;

	private Activity _startingActivity;
	private TextView _timeRemainingLabel;
	private DeviceFunctionality _phone;
	private String _phoneNumber;
	
	public HelpCountdownTimer(
			Activity startingActivity, TextView timeRemaining,
			DeviceFunctionality phone, long cancelWindow	) {
		
		super(cancelWindow, TICK_FREQUENCY);
		
		_startingActivity = startingActivity;
		_timeRemainingLabel = timeRemaining;
		_phone = phone;
	}

	@Override
	public void onFinish() {
		// call the given phone number
		_phone.call(_phoneNumber);
		// terminate the starting activity
		_startingActivity.finish();
		
		// send a notification to all carers
		InTouch.getAssistedDataProvider().getCarers(new ResultCallback<List<UnassistedPerson>>() {
			@Override
			public void done(List<UnassistedPerson> result, InTouchException err) {
				// if there was a problem, deal with it and exit
				if(err != null) {
					err.dealWithIt(_startingActivity);
					return;
				}
				
				// log this
				Log.i("HelpCountdownTimer.onFinish", "Got the list of carers: " + result.size());
				
				// for each carer
				for(UnassistedPerson carer : result) {
					// send a red notification
					InTouch.getProvider().sendNotification(carer, NotificationType.RED, new PassiveCallback() {
						@Override
						public void done(boolean successful,
								InTouchException err) {
							// if the context is no longer relevant, exit
							if(_startingActivity == null) {
								return;
							}
							
							// if there was a problem, deal with it
							if(err != null) {
								err.dealWithIt(_startingActivity);
								return;
							}
							
							// otherwise, show a toast saying the notifications were sent
							Toast.makeText(
										_startingActivity,
										"Notifications were sent to your carers",
										Toast.LENGTH_LONG
									).show();
						}
					});
				}
			}
		});
	}

	@Override
	public void onTick(long millisUntilFinished) {
		// update the TextView with the remaining time (in seconds)
		_timeRemainingLabel.setText(Long.toString(millisUntilFinished / 1000));
	}
	
	public void setPhoneNumberToCall(String phoneNumber) {
		// store that person for later
		_phoneNumber = phoneNumber;
	}
	
	/**
	 * Cancels this timer, and also terminates the starting Activity.
	 */
	public void cancelAndExit() {
		// cancel the timer
		cancel();
		// terminate the starting Activity
		_startingActivity.finish();
	}

}
