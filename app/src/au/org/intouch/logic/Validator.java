package au.org.intouch.logic;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
	
	// minimum allowable size for the password
	public static final int PASSWORD_MINIMUM = 6;

	// static method, returns true if the input string is in email format, or false otherwise
	public static boolean isEmail(String string){
		
		// compile the pattern with the regular expression to check emails
		Pattern email = Pattern.compile("^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$");
		
		Matcher match = email.matcher(string);
		
		// if they match return true
		if (match.find()){
			return true;
		}
		else{
			return false;
		}
		
	}
	
	// matches a basic phone number
	public static boolean isPhone(String string){
		System.out.print(string);
		Pattern phone = Pattern.compile("\\+?[0-9]{5,20}");
		
		Matcher match = phone.matcher(string);
		
		if(match.find()){
			return true;
		}
		else{
			return false;
		}
	}
}
