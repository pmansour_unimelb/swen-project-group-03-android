package au.org.intouch.logic;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Vibrator;
import au.org.intouch.logic.interfaces.DeviceFunctionality;

public class Device implements DeviceFunctionality {
	private static final long serialVersionUID = 5760668542066809039L;

	private static final String NUMBER_PREFIX = "tel:";
	
	private Context context;
	private Vibrator vibrator;
	
	public Device(Context context) {
		this.context = context;
		
		// get the vibrator instance for this context
		this.vibrator =
				(Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
	}
	
	@Override
	public void call(String number) {
		Intent callingIntent;
		String formattedNumber;
		
		// create a new calling Intent
		callingIntent = new Intent(Intent.ACTION_CALL);
		
		// format the number to call
		formattedNumber = NUMBER_PREFIX + number;
		// add it to the intent
		callingIntent.setData(Uri.parse(formattedNumber));
		
		// perform the call
		context.startActivity(callingIntent);
	}

	@Override
	public void vibrate(long milliseconds) {
		vibrator.vibrate(milliseconds);
	}
	@Override
	public void vibrate(long[] pattern, int repeatFrom) {
		vibrator.vibrate(pattern, repeatFrom);
	}
	@Override
	public void stopVibrating() {
		vibrator.cancel();
	}

}
