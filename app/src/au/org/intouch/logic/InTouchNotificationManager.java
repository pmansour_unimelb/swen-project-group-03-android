package au.org.intouch.logic;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import au.org.intouch.R;
import au.org.intouch.activities.ProfileActivity;
import au.org.intouch.models.Notification;
import au.org.intouch.models.Notification.NotificationType;

public class InTouchNotificationManager {
	
	private Context _context;
	
	public InTouchNotificationManager(Context context) {
		_context = context;
	}
	
	/**
	 * Create an intent leading to the given notification's sender's profile
	 * @param notification
	 * @return
	 */
	private Intent createIntent(Notification notification) {
		Intent intent;
		
		// create the intent to see the user's profile
		intent = new Intent(_context, ProfileActivity.class);
		intent.putExtra(ProfileActivity.EXTRA_USER, notification.getSender());
		intent.putExtra(ProfileActivity.EXTRA_TAB, ProfileActivity.TAB_STATUS);
		
		// return the intent
		return intent;
	}

	/**
	 * Show a dialog when a red notification is received.
	 */
	private void showRedDialog(final Notification notification) {
		// create a new dialog
		new AlertDialog.Builder(_context)
			// its title is from strings.xml
			.setTitle(R.string.notification_red_dialog_title)
			// set its icon to an alert icon
			.setIcon(android.R.drawable.ic_dialog_alert)
			// its message is from strings.xml
			.setMessage(
					_context.getString(
							R.string.notification_red_dialog_body,
							notification.getSender().getFullName()
						)
					)
			// it has a button to call the person
			.setNeutralButton("Call", new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// call the person
					new Device(_context).call(notification.getSender().getPhoneNumber());
					// dismiss the notification
					cancelNotification(notification.getId());
				}
			})
			// and a message to view their profile
			.setPositiveButton("View Profile", new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					// start the user's profile activity
					_context.startActivity(createIntent(notification));

					// dismiss the notification
					cancelNotification(notification.getId());
				}
			})
			// finally, show it!
			.show();
	}
	/**
	 * Create a pending intent with a back-stack leading to the home-screen.
	 * @param notification The notification to create the intent from.
	 */
	private PendingIntent createPendingIntent(Notification notification) {
		TaskStackBuilder stackBuilder;
		
		// create an artificial back stack leading to the home screen
		stackBuilder = TaskStackBuilder.create(_context);
		stackBuilder.addParentStack(ProfileActivity.class);
		// make it go to the user's profile activity
		stackBuilder.addNextIntent(createIntent(notification));
		
		// return a pending intent from it
		return stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
	}
	/**
	 * Create a notification in the notification bar when a yellow or red
	 * notification is received.
	 * @param notification The {@link au.org.intouch.models.Notification} that
	 * was received.
	 * @param intent The pending intent to run when the notification is clicked.
	 */
	private android.app.Notification createAndroidNotification(Notification notification, PendingIntent intent) {
		return new NotificationCompat.Builder(_context)
			// use the intouch icon for the large icon
			.setLargeIcon(
					BitmapFactory.decodeResource(
						_context.getResources(),
						R.drawable.ic_launcher
					)
				)
			// give it an alert icon
			.setSmallIcon(android.R.drawable.ic_dialog_alert)
			// set its icon from the xml
			.setContentTitle(
					_context.getString(
						// give the appropriate string for the color
						notification.getType() == NotificationType.RED ?
							R.string.notification_red_notification_title :
							R.string.notification_yellow_notification_title,
						notification.getSender().getFullName()
					)
				)
			// set its text from the xml
			.setContentText(
					_context.getString(
						// give the appropriate string for the color
						notification.getType() == NotificationType.RED ?
							R.string.notification_red_notification_body :
							R.string.notification_yellow_notification_body,
						notification.getSender().getFullName()
					)
				)
			// when it's clicked, it should execute the given intent
			.setContentIntent(intent)
			// set the time it was created
			.setWhen(notification.getTime().getTime())
			// we don't want it to remain after the user clicks it
			.setAutoCancel(true)
			
			.build();
	}
	
	/**
	 * Show the given notification with the given tag.
	 * @param notification The notification to show.
	 * @param tag The tag to associate the notification with.
	 */
	private void showNotification(android.app.Notification notification, String tag) {
		NotificationManager manager;
		
		// get the notification service
		manager = (NotificationManager) _context.getSystemService(Context.NOTIFICATION_SERVICE);
		
		// show it at the given (tag, 0) pair
		manager.notify(tag, 0, notification);
		
	}
	/**
	 * Dismiss the notification at (tag, 0).
	 * @param tag The tag to use to find the notification.
	 */
	private void cancelNotification(String tag) {
		NotificationManager manager;
		
		// get the notification service
		manager = (NotificationManager) _context.getSystemService(Context.NOTIFICATION_SERVICE);
		
		// cancel the notification
		manager.cancel(tag, 0);
	}
	
	private void showRedNotification(Notification notification) {
		// show an android notification
		showNotification(
				// create it first
				createAndroidNotification(
					// for the given notification
					notification,
					// going to the given pending intent
					createPendingIntent(notification)
				),
				// its tag will be the notification's id
				notification.getId()
			);
		
		// show an intrusive dialog
		showRedDialog(notification);
	}
	private void showYellowNotification(Notification notification) {
		// show an android notification
		showNotification(
				// create it first
				createAndroidNotification(
					// for the given notification
					notification,
					// going to the given pending intent
					createPendingIntent(notification)
				),
				// its tag will be the notification's id
				notification.getId()
			);
	}
	private void showGreenNotification(Notification notification) {
		// do nothing!
	}
	public void showNotification(Notification notification) {
		switch(notification.getType()) {
			case RED:
				showRedNotification(notification);
				break;
			case YELLOW:
				showYellowNotification(notification);
				break;
			case GREEN:
				showGreenNotification(notification);
				break;
		}
	}
}
