package au.org.intouch;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
import au.org.intouch.activities.HelpTimeoutActivity;
import au.org.intouch.activities.MainActivity;
import au.org.intouch.framework.exceptions.InTouchException;
import au.org.intouch.framework.interfaces.AuthenticationProvider.UserType;
import au.org.intouch.framework.interfaces.ResultCallback;
import au.org.intouch.models.UnassistedPerson;

public class AssistanceAppWidgetProvider extends AppWidgetProvider {

	public static void updateAllWidgets(final Context context) {
		Intent intent;
		
		// create the intent and set its action
		intent = new Intent(context, AssistanceAppWidgetProvider.class);
		intent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
		
		// put the widget ids into the intent
		intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, getWidgetIds(context));
		
		// send this update broadcast
		context.sendBroadcast(intent);
	}
	
	public static int[] getWidgetIds(Context context) {
		return AppWidgetManager
				.getInstance(context)
				.getAppWidgetIds(
						new ComponentName(context, AssistanceAppWidgetProvider.class)
				);
	}

	public void onUpdate(final Context context, final AppWidgetManager appWidgetManager, final int[] appWidgetIds) {


		Log.v("AssistanceAppWidgetProvider.onUpdate", "Reached onUpdate()");

		Intent intent;
		int layoutId;
		int buttonId;

		// default layout
		layoutId = R.layout.assistance_appwidget;
		// default button
		buttonId = R.id.assistance_appwidget_helpbutton;

		// create the intent to start a new HelpTimeoutActivity
		// if there is no current user logged in, change the layout and go to the dev activity
		if(InTouch.isLoggedIn()){

			// if the user is an assisted person
			if(InTouch.getLoggedInUserType() == UserType.AssistedPerson){

				// if there is no Primary carer, display a different image and go to the phone's dialpad
				// should open a new activity, currently goes to DevActivity
				InTouch.getAssistedDataProvider().getPrimaryCarer(new ResultCallback<UnassistedPerson>() {
					@Override
					public void done(UnassistedPerson result, InTouchException err) {
						Intent intent;
						int layoutId;
						int buttonId;


						if(result == null) {
							// no carer, display no carer message
							intent = new Intent(Intent.ACTION_DIAL);
							layoutId = R.layout.no_carer_appwidget_info;
							buttonId = R.id.no_carer_appwidget_helpbutton;

						} else {
							// if there is a carer, display the help button
							intent = new Intent(context, HelpTimeoutActivity.class);
							layoutId = R.layout.assistance_appwidget;
							buttonId = R.id.assistance_appwidget_helpbutton;

						}

						// loop through the widgets and set them to the appropriate layout
						loopWidgets(intent, layoutId, buttonId, context, appWidgetManager, appWidgetIds);
					}
				});
			
			// if the user is not assisted
			} else {
				intent = new Intent(context, MainActivity.class);
				// add the flags to clear the backstack
				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				layoutId = R.layout.not_assisted_appwidget;
				buttonId = R.id.not_assisted_helpbutton;

				// loop through the widgets and set them to the appropriate layout
				loopWidgets(intent, layoutId, buttonId, context, appWidgetManager, appWidgetIds);
			}

		// no user logged in, go to login activity
		} else {
			intent = new Intent(context, MainActivity.class);
			// add the flags to clear the backstack
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			layoutId = R.layout.no_login_appwidget_info;
			buttonId = R.id.no_login_appwidget_helpbutton;

			// loop through the widgets and set them to the appropriate layout
			loopWidgets(intent, layoutId, buttonId, context, appWidgetManager, appWidgetIds);
		}

	}

	// method to loop through all the widgets that need to be displayed
	private void loopWidgets(Intent intent, int layoutId, int buttonId, final Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds){

		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

		// Perform this loop procedure for each App Widget that belongs to this provider
		for (int i=0; i<appWidgetIds.length; i++) {
			int appWidgetId = appWidgetIds[i];

			// Get the layout for the App Widget and attach an on-click listener
			// to the button
			RemoteViews views = new RemoteViews(context.getPackageName(), layoutId);

			views.setOnClickPendingIntent(buttonId, pendingIntent);

			// Tell the AppWidgetManager to perform an update on the current app widget
			appWidgetManager.updateAppWidget(appWidgetId, views);
		}
	}

}