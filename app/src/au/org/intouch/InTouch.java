package au.org.intouch;

import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.PushService;

import com.testflightapp.lib.TestFlight;

import android.app.Application;
import android.content.SharedPreferences;

import au.org.intouch.activities.MainActivity;
import au.org.intouch.framework.exceptions.InTouchException;
import au.org.intouch.framework.interfaces.AssistedDataProvider;
import au.org.intouch.framework.interfaces.AuthenticationProvider;
import au.org.intouch.framework.interfaces.AuthenticationProvider.UserType;
import au.org.intouch.framework.interfaces.DataProvider;
import au.org.intouch.framework.interfaces.PassiveCallback;
import au.org.intouch.framework.interfaces.ResultCallback;
import au.org.intouch.framework.interfaces.UnassistedDataProvider;
import au.org.intouch.framework.parse.ParseAuthenticationProvider;
import au.org.intouch.models.AssistedPerson;
import au.org.intouch.models.UnassistedPerson;

public class InTouch extends Application {
	
	public static final String PC_PREF = "PrimaryCarerPrefs";
	public static final String PC_NAME = "fullname";
	public static final String PC_NUMBER = "phonenumber";

	public static final String PARSE_APPLICATION_ID = "LSdKsitGDcwZEl8dfnoZbeXq5IJeOlTicWfHrdga";
	public static final String PARSE_CLIENT_KEY = "RfTcu5Cuy9Bv3xzRYrASN4L0sqVV7ISR9F2ruiUQ";
	
	public static final String TESTFLIGHT_APP_TOKEN = "c4e2372a-e632-46a4-b453-61f17c78e50d";
	
	private static AuthenticationProvider _authenticator;
	private static InTouch _context;
	
	/** Get the type of user of the logged in user. */
	public static UserType getLoggedInUserType() {
		return
				getProvider().getLoggedInUser() instanceof AssistedPerson ?
						UserType.AssistedPerson :
						UserType.UnassistedPerson;
	}
	/**
	 * Get the general data provider in use - regardless of the type of the
	 * logged in user.
	 * 
	 * If there is no logged in user, returns null.
	 */
	public static DataProvider getProvider() {
		return _authenticator.getProvider();
	}
	/**
	 * Gets the data provider in use for an assisted person.
	 * 
	 * If the logged in user is not an assisted person, throws an error.
	 * If there is no logged in user, returns null.
	 */
	public static AssistedDataProvider getAssistedDataProvider() {
		return (AssistedDataProvider) getProvider();
	}
	/**
	 * Gets the data provider in use for an unassisted person.
	 * 
	 * If the logged in user is not an unassisted person, throws an error.
	 * If there is no logged in user, throws an error.
	 */
	public static UnassistedDataProvider getUnassistedDataProvider() {
		return (UnassistedDataProvider) getProvider();
	}
	
	private Initializer init;
	
	@Override
    public void onCreate() {
        super.onCreate();
        
        // create a new initializer and initialize everything
        init = new Initializer(this);
        init.initAll();
        
        // create a new instance of a ParseAuthentication class
        _authenticator = new ParseAuthenticationProvider();

        // initialize parse and test flight
        Parse.initialize(this, PARSE_APPLICATION_ID, PARSE_CLIENT_KEY);
        TestFlight.takeOff(this, TESTFLIGHT_APP_TOKEN);
        
        _context = (InTouch) getApplicationContext();
        
        // inform the Parse Push Service that it is ready for notifications
        PushService.setDefaultPushCallback(this, MainActivity.class);
        
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        if (ParseUser.getCurrentUser() != null) {
            installation.put("owner", ParseUser.getCurrentUser());
        }
        installation.saveInBackground();
        
    }
	
	/**
	 * Update the stored information about the primary carer.
	 * If the given object is null, will clear all info we have
	 * about the PC.
	 */
	public static void updatePCInfo(UnassistedPerson primaryCarer) {
		SharedPreferences.Editor editor;
		String name, number;
		
		// get the primary carer shared preferences editor
		editor = _context.getSharedPreferences(PC_PREF, MODE_PRIVATE).edit();
		
		// if we're given null, clear the details
		if(primaryCarer == null) {
			name = "";
			number = "";
		// otherwise, use the details from the given primary carer
		} else {
			name = primaryCarer.getFullName();
			number = primaryCarer.getPhoneNumber();
		}
		
		// add the primary carer's full name and phone number
		editor.putString(PC_NAME, name);
		editor.putString(PC_NUMBER, number);
		
		// try to commit the changes
		editor.commit();
	}
	
	/**
	 * This method is called whenever a user logs into the app.
	 */
	public static void onLogin() {
		// update the widgets
		AssistanceAppWidgetProvider.updateAllWidgets(_context);
		// if this is an assisted person, add the primary carer to
		// a shared preferences
		if(getLoggedInUserType() == UserType.AssistedPerson) {
			getAssistedDataProvider().getPrimaryCarer(new ResultCallback<UnassistedPerson>() {
				@Override
				public void done(UnassistedPerson result, InTouchException err) {
					// if there was a problem
					if(err != null) {
						// deal with it!
						err.dealWithIt(_context);
						// exit
						return;
					}
					
					// otherwise, update the stored information about the PC
					updatePCInfo(result);
				}
			});
		}
	}
	/**
	 * This method is called whenever a user logs out of the app.
	 */
	public static void onLogout() {
		// update the widgets
		AssistanceAppWidgetProvider.updateAllWidgets(_context);
		
		// clear any info we have about the primary carer
		updatePCInfo(null);
	}
	
	public static void logout (final PassiveCallback callback) {
		_authenticator.logout(new PassiveCallback() {
			
			@Override
			public void done(boolean successful, InTouchException err) {
				callback.done(successful, err);
				// call the on logout handler
				onLogout();
			}
		});
	}
	
	public static void login (String email, String password, final PassiveCallback callback) {
		_authenticator.login(email, password, new PassiveCallback() {
			@Override
			public void done(boolean successful, InTouchException err) {
				// call the callback of whatever called this method
				callback.done(successful, err);
				// if it was successful, call the on login handler
				if(successful) {
					onLogin();
				}
			}
		});
		
	}

	public static boolean isLoggedIn() {
		return _authenticator.isLoggedIn();
	}

	public static void register(String email, String password, String firstName,
			String lastName, String phoneNumber, UserType userType,
			final PassiveCallback callback) {
		_authenticator.register(email, password, firstName, lastName, phoneNumber, userType, new PassiveCallback() {
			
			@Override
			public void done(boolean successful, InTouchException err) {
				callback.done(successful, err);
				// if it was successful, call the on login handler
				if(successful) {
					onLogin();
				}
			}
		});
		
	}
}
