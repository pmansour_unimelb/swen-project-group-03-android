package au.org.intouch.framework.parse;

import java.util.Date;

import com.parse.ParseUser;

import au.org.intouch.framework.exceptions.InTouchException;
import au.org.intouch.framework.exceptions.Problem;
import au.org.intouch.framework.interfaces.AuthenticationProvider;
import au.org.intouch.framework.interfaces.DataProvider;
import au.org.intouch.framework.interfaces.PassiveCallback;
import au.org.intouch.models.AssistedPerson;
import au.org.intouch.models.UnassistedPerson;
import au.org.intouch.models.User;

public class ParseAuthenticationProvider implements AuthenticationProvider {

	// private instance variables
	
	private DataProvider _data;
	
	// overriden methods
	
	@Override
	public void register(	String email, String password,
							String firstName, String lastName,
							String phoneNumber, UserType userType,
							PassiveCallback callback) {
		final ParseUser u;
		
		// create a new parse user for this user
		u = new ParseUser();

		// set the username to be the email, and the other fields accordingly
		u.setUsername(email);
		u.setPassword(password);
		u.setEmail(email);
		
		// add the user type
		u.put(ParseEntityAdapter.User_Assisted, userType == UserType.AssistedPerson);
		
		// add the name and phone number
		u.put(ParseEntityAdapter.User_FirstName, firstName);
		u.put(ParseEntityAdapter.User_LastName, lastName);
		u.put(ParseEntityAdapter.User_PhoneNumber, phoneNumber);
		u.put(ParseEntityAdapter.User_LIT, new Date());
		u.put(ParseEntityAdapter.User_LCT, new Date());
		
		// perform the sign up asynchronously
		u.signUpInBackground(ParseCallbackAdapter.toSignUpCallback(callback));
	}

	@Override
	public void login(String email, String password,
			PassiveCallback callback) {
		// call the corresponding ParseUser method
		ParseUser.logInInBackground(
					email,
					password,
					ParseCallbackAdapter.toLoginCallback(callback)
				);
	}

	@Override
	public void logout(PassiveCallback callback) {
		// try to call the corresponding ParseUser method
		try {
			ParseUser.logOut();
			// if logout succeeded, give a success
			callback.done(true, null);
		} catch(Exception e) {
			// otherwise, give a fail
			callback.done(
						false,
						new InTouchException(Problem.LogoutOther, e)
					);
		} finally {
			// remove the current data provider
			_data = null;
		}
	}

	@Override
	public boolean isLoggedIn() {
		// is there a current cached user?
		return ParseUser.getCurrentUser() != null;
	}

	@Override
	public DataProvider getProvider() {
		User loggedInUser;
		
		// if there is no existing data provider but the user is logged in
		if(_data == null && isLoggedIn()) {
			// load the logged in user
			loggedInUser = ParseEntityAdapter.fromParseObject(ParseUser.getCurrentUser(), User.class);
			
			// create a new data provider based on the logged in user type
			if(loggedInUser instanceof AssistedPerson) {
				_data = new ParseAssistedDataProvider();
			} else if(loggedInUser instanceof UnassistedPerson) {
				_data = new ParseUnassistedDataProvider();
			}
		}
		
		// return the current data provider
		return _data;
	}

	@Override
	public void requestPasswordReset(String email,
				PassiveCallback callback) {
		// call the corresponding ParseUser method
		ParseUser.requestPasswordResetInBackground(
				email,
				ParseCallbackAdapter.toRequestPasswordResetCallback(callback)
			);
	}

}
