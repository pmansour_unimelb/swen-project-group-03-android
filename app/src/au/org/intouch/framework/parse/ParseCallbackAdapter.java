package au.org.intouch.framework.parse;

import java.util.ArrayList;
import java.util.List;

import au.org.intouch.framework.exceptions.InTouchException;
import au.org.intouch.framework.exceptions.Problem;
import au.org.intouch.framework.interfaces.PassiveCallback;
import au.org.intouch.framework.interfaces.ResultCallback;
import au.org.intouch.models.Entity;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;

/**
 * A class that just contains static methods for converting between Parse
 * callback methods and our own Passive/Result Callbacks.
 * @author peter
 *
 */
public final class ParseCallbackAdapter {
	
	/** Private constructor so no-one tries to initialize this class. */
	private ParseCallbackAdapter() {
	}
	
	/**
	 * Convert a {@link PassiveCallback} to a {@link SignUpCallback}.
	 */
	public static SignUpCallback toSignUpCallback(
			final PassiveCallback callback) {
		
		// return a new instance of the requested type of callback
		return new SignUpCallback() {
			@Override
			public void done(ParseException exception) {
				boolean success;
				InTouchException e;
				
				if (callback == null) return;
				
				// if the sign up succeeded, mark it as a success
				if(exception == null) {
					success = true;
					e = null;
				} else {
					// mark it as a fail
					success =  false;
					// get the InTouchException for this ParseException
					e = fromParseException(exception, Problem.RegistrationOther);
				}
				
				// call done on the callback we received
				callback.done(success, e);
			}
		};
	}

	/**
	 * Convert a {@link PassiveCallback} to a {@link LogInCallback}.
	 */
	public static LogInCallback toLoginCallback(
			final PassiveCallback callback) {

		// return a new instance of the requested type of callback
		return new LogInCallback() {
			@Override
			public void done(ParseUser newUser, ParseException exception) {
				boolean success;
				InTouchException e;
				
				if (callback == null) return;
				
				// if the login succeeded, mark it as a success
				if(newUser != null && exception == null) {
					success = true;
					e = null;
				} else {
					// otherwise, mark it as a fail
					success = false;		
					
					// get the InTouchException for this ParseException
					if(exception.getCode() == ParseException.OBJECT_NOT_FOUND) {
						// login incorrect credentials is a special case
						e = new InTouchException(Problem.LoginIncorrectCredentials, exception);
					} else {
						e = fromParseException(exception, Problem.LoginOther);
					}
				}
				
				// call done() on the callback we received
				callback.done(success, e);
			}
		};
	}

	/**
	 * Convert a {@link PassiveCallback} to a {@link RequestPasswordResetCallback}.
	 */
	public static RequestPasswordResetCallback toRequestPasswordResetCallback(
			final PassiveCallback callback) {
		// return a new instance of the requested type of callback
		return new RequestPasswordResetCallback() {
			@Override
			public void done(ParseException exception) {
				boolean success;
				InTouchException e;

				if (callback == null) return;
				
				// if there was no exception thrown, it was a success
				if(exception == null) {
					success = true;
					e = null;
				// if there was a failure,
				} else {
					// mark it as a fail
					success = false;
					// get the InTouchException for this ParseException
					e = fromParseException(exception, Problem.RequestPasswordResetOther);
				}
				
				// call done() on the callback we received
				callback.done(success, e);
			}
		};
	}	
	
	/**
	 * Convert a {@link PassiveCallback} to a {@link SaveCallback}.
	 */
	public static SaveCallback toSaveCallback(
			final PassiveCallback callback) {
		// return a new instance of the requested type of callback
		return new SaveCallback() {
			@Override
			public void done(ParseException exception) {
				boolean success;
				InTouchException e;
				
				if (callback == null) return;
				
				// if there was no exception thrown, it was a success
				if(exception == null) {
					success = true;
					e = null;
				// if there was a failure,
				} else {
					// mark it as a fail
					success = false;
					// get the InTouchException for this ParseException
					e = fromParseException(exception, Problem.SaveOther);
				}
				
				// call done() on the callback we received
				callback.done(success, e);
			}
		};
	}
	
	public static <T extends Entity> FindCallback<ParseObject> toFindCallback(
			final ResultCallback<T> callback, final Class<T> type) {
		return new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> found, ParseException exception) {
				T result;
				InTouchException err;
				
				// if there was an exception,
				if(exception != null) {
					// we have no result
					result = null;
					// ... but we have an exception!
					err = fromParseException(exception, Problem.LoadOther);
				// otherwise,
				} else {
					// convert the result into the requested type
					result = ParseEntityAdapter.fromParseObject(found.get(0), type);					
					// but there was no exception! hooray!
					err = null;
				}
				
				// return the result and the exception generated (if applicable)
				if(callback != null) {
					callback.done(result, err);
				}
			}
		};
	}
	public static <T extends Entity> FindCallback<ParseObject> toFindCallbackList(
			final ResultCallback<List<T>> callback, final Class<T> type) {
		return new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> found, ParseException exception) {
				List<T> results;
				InTouchException err;
				
				// if there was an exception,
				if(exception != null) {
					// we have no results
					results = null;
					// ... but we have an exception!
					err = fromParseException(exception, Problem.LoadOther);
				// otherwise,
				} else {
					// create a new array list for the results
					results = new ArrayList<T>(found.size());
					// convert and add them
					for(ParseObject pEntity : found) {
						results.add(ParseEntityAdapter.fromParseObject(pEntity, type));
					}				
					// but there was no exception! hooray!
					err = null;
				}
				
				// return the results and the exception generated (if applicable)
				if(callback != null) {
					callback.done(results, err);
				}
			}
		};
	}
	
	public static DeleteCallback toDeleteCallback(final PassiveCallback callback) {
		return new DeleteCallback() {
			@Override
			public void done(ParseException exception) {
				// if there was an exception, pass it back up
				if(exception != null) {
					callback.done(false, fromParseException(exception, Problem.FetchOther));
					return;
				}
				
				// otherwise, it's successful!
				if(callback != null) {
					callback.done(true, null);
				}
			}
		};
	}
	
	public static <T extends Entity> GetCallback<ParseObject> toGetCallback(
			final ResultCallback<T> callback, final Class<T> type) {
		return new GetCallback<ParseObject>() {
			@Override
			public void done(ParseObject found, ParseException exception) {
				T result;
				InTouchException err;
				
				// if there was an exception,
				if(exception != null) {
					// we have no result
					result = null;
					// ... but we have an exception!
					err = fromParseException(exception, Problem.LoadOther);
				// otherwise,
				} else {
					// convert the result into the requested type
					result = ParseEntityAdapter.fromParseObject(found, type);					
					// but there was no exception! hooray!
					err = null;
				}
				
				// return the result and the exception generated (if applicable)
				if(callback != null) {
					callback.done(result, err);
				}
			}
		};
	}
	
	public static InTouchException fromParseException(ParseException exception, Problem defaultProblem) {
		Problem p;
		
		// find an appropriate but general problem based on the given exception
		switch(exception.getCode()) {
			case ParseException.USERNAME_MISSING:
			case ParseException.EMAIL_MISSING:
				p = Problem.InvalidEmail;
				break;
			case ParseException.PASSWORD_MISSING:
				p = Problem.InvalidPassword;
				break;
			case ParseException.EMAIL_TAKEN:
				p = Problem.ExistingEmail;
				break;
			case ParseException.CACHE_MISS:
				p = Problem.CacheMiss;
				break;
			default:
				// if we can't find anything, just give back the default problem
				// we've been given
				p = defaultProblem;
				break;
		}
		
		// return an exception with the problem we found
		return new InTouchException(p, exception);
	}
}
