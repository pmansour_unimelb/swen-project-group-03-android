package au.org.intouch.framework.parse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.parse.GetCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.ParseQuery.CachePolicy;

import android.util.Log;
import au.org.intouch.framework.exceptions.InTouchException;
import au.org.intouch.framework.exceptions.Problem;
import au.org.intouch.framework.interfaces.DataProvider;
import au.org.intouch.framework.interfaces.PassiveCallback;
import au.org.intouch.framework.interfaces.ResultCallback;
import au.org.intouch.models.Communication;
import au.org.intouch.models.ContactRequest;
import au.org.intouch.models.Message;
import au.org.intouch.models.Notification;
import au.org.intouch.models.Relation;
import au.org.intouch.models.Role;
import au.org.intouch.models.Notification.NotificationType;
import au.org.intouch.models.User;
import au.org.intouch.receivers.PushReceiver;

public abstract class ParseDataProvider implements DataProvider {

	@Override
	public User getLoggedInUser() {
		ParseObject userObject;
		
		// get the logged in parse user
		userObject = ParseUser.getCurrentUser();
		
		
		// convert that to a user model and return it
		return ParseEntityAdapter.fromParseObject(userObject, User.class);
	}

	@Override
	public void updateUserModel(final User newUserModel,
			final PassiveCallback callback) {
		ParseObject user;
		
		// get the currently logged in parse user
		user = ParseUser.getCurrentUser();
		
		// copy the required attributes there
		ParseEntityAdapter.copyUserValuesToUserParseObject(newUserModel, user);

		// save the user remotely
		user.saveEventually(ParseCallbackAdapter.toSaveCallback(callback));
	}

	@Override
	public void updateEmail(final String email,
			final PassiveCallback callback) {
		final ParseUser user;
		
		// get the currently logged in parse user
		user = ParseUser.getCurrentUser();
		
		// change their parse user email
		user.setEmail(email);
		
		// change their email field
		user.put(ParseEntityAdapter.User_Email, email);
		
		// save the parse user
		user.saveEventually(ParseCallbackAdapter.toSaveCallback(callback));
	}

	@Override
	public void updatePassword(String oldPassword, String newPassword,
			PassiveCallback callback) {
		ParseUser user;
		
		// get the currently logged in parse user
		user = ParseUser.getCurrentUser();
		
		/* do nothing with the old password - they must already be logged in
		 * to even get here and to use getCurrentUser().
		 *
		 * in parse, this is sort of a redundant feature
		 * (maybe could change this in the future) */
		
		// change their password
		user.setPassword(newPassword);
		
		// save the parse user (with the given callback)
		user.saveEventually(ParseCallbackAdapter.toSaveCallback(callback));
	}
	
	@Override
	public void setProfilePicture(String name, byte[] imageData, PassiveCallback callback) {
		ParseUser user;
		ParseFile file;
		
		// get the currently logged in parse user
		user = ParseUser.getCurrentUser();
		
		// create a parse file for this image
		file = new ParseFile(name, imageData);
		
		// set the user's profile image to this file
		user.put(ParseEntityAdapter.User_ProfileImage, file);
		
		// save the parse user
		user.saveEventually(ParseCallbackAdapter.toSaveCallback(callback));
	}

	@Override
	public Message sendMessage(User recipient, String body,
			PassiveCallback callback) {
		ParseObject pMessage;
		ParseUser pRecipient;
		ParseUser pSender;
		ParseACL groupACL;
		Message m;
		
		// create a new message
		m = new Message(null);
		
		// give it the correct fields
		m.setSender(getLoggedInUser());
		m.setRecipient(recipient);
		m.setTime(new Date());
		m.setRead(false);
		m.setBody(body);
		
		// create a new parse object out of it
		pMessage = ParseEntityAdapter.toParseObject(m, true);
		pRecipient = ParseUser.createWithoutData(ParseUser.class, recipient.getId());
		pSender = ParseUser.createWithoutData(ParseUser.class, getLoggedInUser().getId());

		groupACL = new ParseACL();
		groupACL.setReadAccess(pSender, true);
		groupACL.setWriteAccess(pSender, true);

		groupACL.setReadAccess(pRecipient, true);
		groupACL.setWriteAccess(pRecipient, true);

		pMessage.setACL(groupACL);
		
		// save that parse object
		pMessage.saveEventually(ParseCallbackAdapter.toSaveCallback(callback));

		ParseQuery<ParseInstallation> query = ParseInstallation.getQuery();
		query.whereEqualTo("owner", pRecipient);

		ParsePush push = new ParsePush();
		push.setQuery(query);
		push.setMessage(body);
		push.sendInBackground();
		return m;
	}

	@Override
	public void sendNotification(final User recipient, NotificationType type,
			final PassiveCallback callback) {
		final ParseObject pNotification;
		Notification n;
		
		// create a new notification
		n = new Notification(null);
		
		// set the required fields
		n.setSender(getLoggedInUser());
		n.setRecipient(recipient);
		n.setTime(new Date());
		n.setRead(false);
		n.setType(type);
		
		/** Part 1 - Create a parse object and save it. */
		
		// create a new parse object out of it
		pNotification = ParseEntityAdapter.toParseObject(n, true);
		
		// save that parse object
		pNotification.saveEventually(ParseCallbackAdapter.toSaveCallback(new PassiveCallback() {
			@Override
			public void done(boolean successful, InTouchException err) {
				ParseQuery<ParseInstallation> query;
				ParsePush push;
				JSONObject data;
				
				// if there was no problem,
				if(successful) {
					/** Part 2 - Send a push notification. */
					
					// logging
					Log.i("ParseDataProvider.sendNotification", "Notification saved successfully.");
					
					// create a parse query for the parse installation(s) of the recipient
					query = ParseInstallation.getQuery()
								.whereEqualTo(
									ParseEntityAdapter.Installation_Owner,
									ParseUser.createWithoutData(
										ParseEntityAdapter.User_Class,
										recipient.getId()
									)
								);
					
					Log.i("ParseDataProvider.sendNotification", "Query going to user " + recipient.getId());
					
					// create a parse push with this query
					push = new ParsePush();
					push.setQuery(query);
					
					try {
						// create the data for it
						data = new JSONObject();
						// action and notification id
						data.put("action", PushReceiver.INTOUCH_PUSH_ACTION);
						data.put(PushReceiver.DATA_NOTIFICATION_ID, pNotification.getObjectId());
						
						// log the JSON object
						Log.i("ParseDataProvider.sendNotification", "push data: " + data.toString());
					} catch (JSONException e) {
						// print the stack trace and exit
						e.printStackTrace(System.err);
						return;
					}
					
					// add this data to the parse push
					push.setData(data);
					
					// send the push
					push.sendInBackground();
					
					// log this success
					Log.i("ParseDataProvider.sendNotification", "push sent.");
				}
				
				// call the given callback
				if(callback != null) {
					callback.done(successful, err);
				}
			}
		}));
		
		
	}

	@Override
	public void setCommunicationAsRead(Communication communication,
			PassiveCallback callback) {
		ParseObject pCommunication;
		
		// set the given model as read
		communication.setRead(true);
		
		// get its corresponding parse object
		pCommunication = ParseEntityAdapter.toParseObject(communication);
		
		// save it
		pCommunication.saveEventually(ParseCallbackAdapter.toSaveCallback(callback));
	}

	private ParseQuery<ParseObject> getCommunicationsQuery(User withWho) {
		ArrayList<ParseQuery<ParseObject>> queries;
		ParseQuery<ParseObject> query;
		ParseObject loggedInUser;
		ParseObject otherUser;
		
		// get the logged in parse user
		loggedInUser = ParseUser.getCurrentUser();
		
		// create the queries to get the user's communications
		queries = new ArrayList<ParseQuery<ParseObject>>(2);
		
		// if necessary, add a requirement that they're with a certain person
		if(withWho != null) {

			// get the other person's parse object
			otherUser = ParseObject.createWithoutData(ParseEntityAdapter.User_Class, withWho.getId());
			
			queries.add(ParseQuery.getQuery(ParseEntityAdapter.Communication_ClassName)
					.whereEqualTo(
							ParseEntityAdapter.Communication_Sender,
							otherUser
					)
					.whereEqualTo(
							ParseEntityAdapter.Communication_Recipient,
							loggedInUser
					));
			
			// modify the queries to include the other user
			queries.add(ParseQuery.getQuery(ParseEntityAdapter.Communication_ClassName)
					.whereEqualTo(
							ParseEntityAdapter.Communication_Recipient,
							otherUser
					)
					.whereEqualTo(
							ParseEntityAdapter.Communication_Sender,
							loggedInUser
					));
			
		} else {
			queries.add(ParseQuery.getQuery(ParseEntityAdapter.Communication_ClassName)
					.whereEqualTo(
							ParseEntityAdapter.Communication_Sender,
							loggedInUser
					));
			
			queries.add(ParseQuery.getQuery(ParseEntityAdapter.Communication_ClassName)
					.whereEqualTo(
							ParseEntityAdapter.Communication_Recipient,
							loggedInUser
					));
		}
		
		// merge the queries into a big or query
		query = ParseQuery.or(queries);
		
		// include the sender and receiver in the query
		query.include(ParseEntityAdapter.Communication_Sender);
		query.include(ParseEntityAdapter.Communication_Recipient);
		
		// return the resulting query
		return query;
	}
	
	@Override
	public void getUnreadNotifications(ResultCallback<List<Notification>> callback) {
		ParseQuery<ParseObject> query;
		
		// build the query
		query = ParseQuery.getQuery(ParseEntityAdapter.Communication_ClassName)
					.whereEqualTo(
						// we only want notifications towards the logged in user
						ParseEntityAdapter.Communication_Recipient,
						ParseUser.getCurrentUser()
					).whereEqualTo(
						ParseEntityAdapter.Communication_Type,
						ParseEntityAdapter.Communication_Type_Notification
					);
		
		// include both parties
		query.include(ParseEntityAdapter.Communication_Sender);
		query.include(ParseEntityAdapter.Communication_Recipient);
		query.setCachePolicy(CachePolicy.NETWORK_ONLY);
		
		// execute it in background
		query.findInBackground(ParseCallbackAdapter.toFindCallbackList(callback, Notification.class));
	}
	
	@Override
	public void getCommunications(
			final ResultCallback<List<Communication>> callback) {
		ParseQuery<ParseObject> query;
		
		// get a query for communications of the logged in user with anyone else
		query = getCommunicationsQuery(null);
		query.setCachePolicy(CachePolicy.CACHE_THEN_NETWORK);
		// execute it in background, and add an appropriate handler
		query.findInBackground(ParseCallbackAdapter.toFindCallbackList(callback, Communication.class));
	}

	/**
	 * Note: startIndex doesn't work here!
	 * 
	 * Note2: in order to avoid code repitition, this method is used for all the
	 * getConversations() overloads. If endIndex <= 0, then it gets them all.
	 */
	@Override
	public void getConversations(User withWho, int startIndex, int endIndex,
			ResultCallback<List<Communication>> callback) {
		ParseQuery<ParseObject> query;
		
		// get a query for communications of the logged in user with the given user
		query = getCommunicationsQuery(withWho);
		query.setCachePolicy(CachePolicy.NETWORK_ELSE_CACHE);
		
		// add a limit if needed
		if(endIndex > 0) {
			query.setLimit(endIndex);
		}

		// execute it in background, and add an appropriate handler
		query.findInBackground(ParseCallbackAdapter.toFindCallbackList(callback, Communication.class));
	}

	@Override
	public void getConversations(User recipient, int limit,
			ResultCallback<List<Communication>> callback) {
		getConversations(recipient, -1, limit, callback);
	}

	@Override
	public void getConversations(User recipient,
			ResultCallback<List<Communication>> callback) {
		// call the overload with limit 0
		getConversations(recipient, -1, callback);
	}

	@Override
	public void getContacts(
			final ResultCallback<List<Relation>> callback) {
		ParseQuery<ParseObject> query;
		
		// build a query to get all the relations that the user is involved in (all roles)
		query = getRelationsQuery(null);
		
		// set some properties of the query
		query.setCachePolicy(CachePolicy.CACHE_THEN_NETWORK);
		query.include(ParseEntityAdapter.Relation_AssistedPerson);
		query.include(ParseEntityAdapter.Relation_UnassistedPerson);
		
		// execute the query asynchronously
		query.findInBackground(ParseCallbackAdapter.toFindCallbackList(callback, Relation.class));
	}

	@Override
	public void sendContactRequest(String recipientEmail, Role desiredRole,
			PassiveCallback callback) {
		ContactRequest request;
		ParseObject pRequest;
		
		// create a new contact request
		request = new ContactRequest(null);
		
		// set the initiator, recipient email, and desired role
		request.setInitiator(getLoggedInUser());
		request.setRecipientEmail(recipientEmail);
		request.setDesiredRole(desiredRole);
		
		// convert it to a parse object
		pRequest = ParseEntityAdapter.toParseObject(request, true);
		
		// save it with the given callback
		pRequest.saveEventually(ParseCallbackAdapter.toSaveCallback(callback));
	}
	
	@Override
	public void removeContact(User contact, final PassiveCallback callback) {
		ParseUser loggedInUser, userToRemove;
		ParseQuery<ParseObject> relationQuery;
		ParseUser ap, up;
		
		// get the logged in user and the user we want to remove
		loggedInUser = ParseUser.getCurrentUser();
		userToRemove = (ParseUser) ParseEntityAdapter.toParseObject(contact, false);
		
		// based on the type of the logged in user, figure out which user
		// is assisted and which is unassisted
		if(loggedInUser.getBoolean(ParseEntityAdapter.User_Assisted)) {
			ap = loggedInUser;
			up = userToRemove;
		} else {
			ap = userToRemove;
			up = loggedInUser;
		}
		
		// make a query for the relation that binds them
		relationQuery = ParseQuery.getQuery(ParseEntityAdapter.Relation_ClassName)
							.whereEqualTo(ParseEntityAdapter.Relation_AssistedPerson, ap)
							.whereEqualTo(ParseEntityAdapter.Relation_UnassistedPerson, up);
		
		// get it
		relationQuery.getFirstInBackground(new GetCallback<ParseObject>() {
			@Override
			public void done(ParseObject relation, ParseException exception) {
				// if there was a problem, pass it back up
				if(exception != null) {
					callback.done(
							false,
							ParseCallbackAdapter.fromParseException(exception, Problem.LoadOther)
						);
					return;
				}
				
				// otherwise, try to delete that relation that we just fetched!
				relation.deleteEventually(ParseCallbackAdapter.toDeleteCallback(callback));
			}
		});
	}
	
	protected abstract ParseQuery<ParseObject> getRelationsQuery(Role role);

	@Override
	public void getContactRequests(final ResultCallback<List<ContactRequest>> callback) {
		ParseQuery<ParseObject> query;
		
		// build a query to get the user's assistance offers
		query = ParseQuery.getQuery(ParseEntityAdapter.ContactRequest_ClassName)
					.whereEqualTo(
							ParseEntityAdapter.ContactRequest_RecipientEmail,
							getLoggedInUser().getEmail()
					).whereEqualTo(
							ParseEntityAdapter.ContactRequest_Responded,
							false
					);
		query.include(ParseEntityAdapter.ContactRequest_Initiator);
		
		// execute it asynchronously
		query.findInBackground(ParseCallbackAdapter.toFindCallbackList(callback, ContactRequest.class));	
	}

	@Override
	public void updateContactRequest(ContactRequest request,
			PassiveCallback callback) {
		ParseObject p = ParseEntityAdapter.toParseObject(request);
		p.saveInBackground(ParseCallbackAdapter.toSaveCallback(callback));
	}
	
	@Override
	public void getNotificationById(String id, ResultCallback<Notification> callback) {
		ParseObject pNotification;
		
		// create and fetch the notification's existing parse object
		pNotification = ParseObject.createWithoutData(ParseEntityAdapter.Communication_ClassName, id);
		
		// get that object to the given callback
		pNotification.fetchInBackground(ParseCallbackAdapter.toGetCallback(callback, Notification.class));
	}

}
