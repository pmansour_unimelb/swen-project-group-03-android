package au.org.intouch.framework.parse;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import au.org.intouch.framework.interfaces.UnassistedDataProvider;
import au.org.intouch.models.Role;

public class ParseUnassistedDataProvider extends ParseDataProvider implements UnassistedDataProvider {

	@Override
	protected ParseQuery<ParseObject> getRelationsQuery(Role role) {
		ParseQuery<ParseObject> query;
		
		// get the general query
		query = ParseQuery.getQuery(ParseEntityAdapter.Relation_ClassName)
					.whereEqualTo(
							ParseEntityAdapter.Relation_UnassistedPerson,
							ParseUser.getCurrentUser()
					);
		
		// include the assisted person with the query
		query.include(ParseEntityAdapter.Relation_AssistedPerson);
		query.include(ParseEntityAdapter.Relation_UnassistedPerson);
		
		// if there is a specific role we're looking for, add it as a constraint
		if(role != null) {
			query = query.whereEqualTo(ParseEntityAdapter.Relation_Role, role.toString());
		}
		
		// return the query
		return query;
	}

	
}
