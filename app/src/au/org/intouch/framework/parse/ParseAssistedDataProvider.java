package au.org.intouch.framework.parse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQuery.CachePolicy;
import com.parse.ParseUser;

import au.org.intouch.framework.exceptions.Problem;
import au.org.intouch.framework.interfaces.AssistedDataProvider;
import au.org.intouch.framework.interfaces.PassiveCallback;
import au.org.intouch.framework.interfaces.ResultCallback;
import au.org.intouch.models.Role;
import au.org.intouch.models.UnassistedPerson;

public class ParseAssistedDataProvider extends ParseDataProvider implements AssistedDataProvider {
	
	@Override
	protected ParseQuery<ParseObject> getRelationsQuery(Role role) {
		ParseQuery<ParseObject> query;
		
		// get the general query
		query = ParseQuery.getQuery(ParseEntityAdapter.Relation_ClassName)
					.whereEqualTo(
							ParseEntityAdapter.Relation_AssistedPerson,
							ParseUser.getCurrentUser()
					);
		// include the unassisted person in the query
		query.include(ParseEntityAdapter.Relation_AssistedPerson);
		query.include(ParseEntityAdapter.Relation_UnassistedPerson);
		
		// if there is a specific role we're looking for, add it as a constraint
		if(role != null) {
			query = query.whereEqualTo(ParseEntityAdapter.Relation_Role, role.toString());
		}
		
		// return the query
		return query;
	}
	
	@Override
	public void getPrimaryCarer(final ResultCallback<UnassistedPerson> callback) {
		ParseQuery<ParseObject> query;
		
		
		// build a query to get the user's primary carer relation
		query = getRelationsQuery(Role.PrimaryCarer);
		// set cache policy https://parse.com/docs/android_guide#queries-caching
		query.setCachePolicy(CachePolicy.NETWORK_ELSE_CACHE);
		// execute it asynchronously
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> results, ParseException exception) {
				ParseObject relation;
				UnassistedPerson primaryCarer;
				
				// if there was a problem,
				if(exception != null) {
					// translate that exception and pass it back up
					callback.done(
							null,
							ParseCallbackAdapter.fromParseException(exception, Problem.LoadOther)
						);
					return;
				}
				
				// otherwise,
				
				// if there is no primary carer, just call done with null
				if(results.size() == 0) {
					callback.done(null, null);
					return;
				}
				
				// otherwise, get the first (and only) result
				relation = results.get(0);

				// get the user model for that primary carer
				primaryCarer = ParseEntityAdapter.fromParseObject(
								relation.getParseObject(ParseEntityAdapter.Relation_UnassistedPerson),
								UnassistedPerson.class
							);

				// give back the primary carer we got
				callback.done(primaryCarer, null);
			}
		});		
	}

	@Override
	public void getCarers(
			final ResultCallback<List<UnassistedPerson>> callback) {
		ParseQuery<ParseObject> query;
		
		// build a query to get the user's carer relations
		query = getRelationsQuery(Role.Carer);

		query.setCachePolicy(CachePolicy.CACHE_THEN_NETWORK);
		// execute it asynchronously
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> results, ParseException exception) {
				ArrayList<UnassistedPerson> carers;
				
				// if there was a problem,
				if(exception != null) {
					// translate that exception and pass it back up
					callback.done(
							null,
							ParseCallbackAdapter.fromParseException(exception, Problem.LoadOther)
						);
					return;
				// otherwise,
				} else {
					// initialize the carers array
					carers = new ArrayList<UnassistedPerson>(results.size());
					// for each carer
					for(ParseObject relation : results) {
						// get the user model for that carer
						carers.add(ParseEntityAdapter.fromParseObject(
								relation.getParseObject(ParseEntityAdapter.Relation_UnassistedPerson),
								UnassistedPerson.class
							));
					}
					
					// give back the list of carers we got
					callback.done(carers, null);
				}
			}
		});	
	}

	@Override
	public void getFriends(
			final ResultCallback<List<UnassistedPerson>> callback) {
		ParseQuery<ParseObject> query;
		
		// build a query to get the user's friend relations
		query = getRelationsQuery(Role.Friend);
		query.setCachePolicy(CachePolicy.CACHE_THEN_NETWORK);
		// execute it asynchronously
		query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> results, ParseException exception) {
				ArrayList<UnassistedPerson> friends;
				
				// if there was a problem,
				if(exception != null) {
					// translate that exception and pass it back up
					callback.done(
							null,
							ParseCallbackAdapter.fromParseException(exception, Problem.LoadOther)
						);
					return;
				// otherwise,
				} else {
					// initialize the friends array
					friends = new ArrayList<UnassistedPerson>(results.size());
					// for each friend
					for(ParseObject relation : results) {
						// get the user model for that friend
						friends.add(ParseEntityAdapter.fromParseObject(
								relation.getParseObject(ParseEntityAdapter.Relation_UnassistedPerson),
								UnassistedPerson.class
							));
					}
					
					// give back the list of friends we got
					callback.done(friends, null);
				}
			}
		});	
	}

	@Override
	public void updateLastCommunicationTime(PassiveCallback callback) {
		ParseUser pUser;
		Date now;
		
		// get the time now
		now = new Date();
		
		// get the logged in parse user
		pUser = ParseUser.getCurrentUser();
		
		// set their last communication time to now
		pUser.put(ParseEntityAdapter.User_LCT, now);
		// (implicitly, this means they interacted with their phone)
		pUser.put(ParseEntityAdapter.User_LIT, now);
		
		// save
		pUser.saveEventually(ParseCallbackAdapter.toSaveCallback(callback));
	}

	@Override
	public void updateLastInteractionTime(PassiveCallback callback) {
		ParseUser pUser;
		
		// get the logged in parse user
		pUser = ParseUser.getCurrentUser();
		
		// set their last interaction time to now
		pUser.put(ParseEntityAdapter.User_LIT, new Date());
		
		// save
		pUser.saveEventually(ParseCallbackAdapter.toSaveCallback(callback));
	}

	
}
