package au.org.intouch.framework.parse;

import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

import au.org.intouch.framework.interfaces.AuthenticationProvider.UserType;
import au.org.intouch.models.AssistedPerson;
import au.org.intouch.models.Communication;
import au.org.intouch.models.ContactRequest;
import au.org.intouch.models.Entity;
import au.org.intouch.models.Message;
import au.org.intouch.models.Notification;
import au.org.intouch.models.Relation;
import au.org.intouch.models.Role;
import au.org.intouch.models.UnassistedPerson;
import au.org.intouch.models.User;
import au.org.intouch.models.Notification.NotificationType;

public class ParseEntityAdapter {
	protected static final String Installation_Owner = "owner";
	
	protected static final String Communication_ClassName = "Communication";
	protected static final String Communication_Type = "type";
	protected static final String Communication_Type_Message = "message";
	protected static final String Communication_Type_Notification = "notification";
	protected static final String Communication_Sender = "sender";
	protected static final String Communication_Recipient = "recipient";
	protected static final String Communication_Time = "time";
	protected static final String Communication_Read = "read";
	protected static final String Communication_Message_Body = "body";
	protected static final String Communication_Notification_Type = "notificationType";
	
	protected static final String ContactRequest_ClassName = "ContactRequest";
	protected static final String ContactRequest_Type = "type";
	protected static final String ContactRequest_Type_Offer = "offer";
	protected static final String ContactRequest_Type_Request = "request";
	protected static final String ContactRequest_Initiator = "initiator";
	protected static final String ContactRequest_Responded = "responded";
	protected static final String ContactRequest_Accepted = "accepted";
	protected static final String ContactRequest_RecipientEmail = "email";
	protected static final String ContactRequest_DesiredRole = "role";
	
	protected static final String Relation_ClassName = "Relation";
	protected static final String Relation_AssistedPerson = "RelationAssistedPerson";
	protected static final String Relation_UnassistedPerson = "RelationUnassistedPerson";
	protected static final String Relation_Role = "RelationRole";
	
	protected static final Class<? extends ParseObject> User_Class = ParseUser.class;
	protected static final String User_Assisted = "assisted";
	protected static final String User_FirstName = "firstName";
	protected static final String User_LastName = "lastName";
	protected static final String User_Email = "email";
	protected static final String User_PhoneNumber = "phoneNumber";
	protected static final String User_ProfileImage = "image";
	protected static final String User_LCT = "lastCommunicationTime";
	protected static final String User_LIT = "lastDeviceUsedTime";

	

	/**
	 * Convert a given ParseObject to a given Entity type.
	 * @param pEntity The ParseObject to convert.
	 * @param type The type of the returned Entity.
	 * @return The entity generated from the given ParseObject, or null if it's
	 * given a class type it doesn't know.
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Entity> T fromParseObject(ParseObject pEntity, Class<T> type)
			{
		// react based on the expected type
		if(Communication.class.isAssignableFrom(type)) {
			return (T) toCommunication(pEntity);
		} else if(ContactRequest.class.isAssignableFrom(type)) {
			return (T) toContactRequest(pEntity);
		} else if(Relation.class.isAssignableFrom(type)) {
			return (T) toRelation(pEntity);
		} else if(User.class.isAssignableFrom(type)) {
			return (T) toUser(pEntity);
		} else {
			return null;
		}
	}
	
	/**
	 * Convert a given [already existant on the server] entity to a ParseObject.
	 * @param entity The entity to be converted.
	 * @return The (unfetched) ParseObject generated from the given entity, or
	 * null if the entity type was unknown.
	 */
	public static <T extends Entity> ParseObject toParseObject(T entity) {
		return toParseObject(entity, false);
	}
	/**
	 * Convert a given Entity to a ParseObject.
	 * @param entity The entity to be converted.
	 * @param fromNew Does this entity already exist on the server?
	 * @return The (unfetched) ParseObject generated from the given entity, or
	 * null if the entity type was unknown.
	 */
	public static <T extends Entity> ParseObject toParseObject(T entity, boolean fromNew) {
		// react based on the given entity type
		if(entity instanceof Communication) {
			return fromCommunication((Communication) entity, fromNew);
		} else if(entity instanceof ContactRequest) {
			return fromContactRequest((ContactRequest) entity, fromNew);
		} else if(entity instanceof Relation) {
			return fromRelation((Relation) entity, fromNew);
		} else if(entity instanceof User) {
			return fromUser((User) entity, fromNew);
		} else {
			return null;
		}
	}
	
	public static void setParseUserType(ParseObject user, UserType type) {
		if(type == UserType.AssistedPerson) {
			user.put(User_Assisted, true);
		} else if(type == UserType.UnassistedPerson) {
			user.put(User_Assisted, false);
		}
	}
	public static void copyUserValuesToUserParseObject(User source, ParseObject destination) {
		destination.put(User_FirstName, source.getFirstName());
		destination.put(User_LastName, source.getLastName());
		destination.put(User_Email, source.getEmail());
		destination.put(User_PhoneNumber, source.getPhoneNumber());
	}	
	
	// Entity -> ParseObject
	
	private static ParseObject fromUser(User entity, boolean createNew) {
		ParseObject pUser;
		
		// create a new parse user with this class name & id
		if(createNew) {
			pUser = ParseObject.create(User_Class);
		} else {
			pUser = ParseObject.createWithoutData(User_Class, entity.getId());
		}
		
		// add its general fields
		pUser.put(User_FirstName, entity.getFirstName());
		pUser.put(User_LastName, entity.getLastName());
		pUser.put(User_Email, entity.getEmail());
		pUser.put(User_PhoneNumber, entity.getPhoneNumber());
		
		// add its specific fields
		if(entity instanceof AssistedPerson) {
			AssistedPerson ap;
			
			// cast the user into an assisted person
			ap = (AssistedPerson) entity;
			
			// add the user type
			pUser.put(User_Assisted, true);
			
			// add the monitors if they're not null
			if(ap.getLastCommunicationTime() != null) {
				pUser.put(User_LCT, ((AssistedPerson) entity).getLastCommunicationTime());
			}
			if(ap.getLastInteractionTime() != null) {
				pUser.put(User_LIT, ((AssistedPerson) entity).getLastInteractionTime());
			}
		} else {
			// add the user type
			pUser.put(User_Assisted, false);
		}
		
		// return the generated parse user
		return pUser;
	}
	private static ParseObject fromRelation(Relation entity, boolean createNew) {
		ParseObject pRelation;
		
		// create a new parse object with this class name & id
		if(createNew) {
			pRelation = ParseObject.create(Relation_ClassName);
		} else {
			pRelation = ParseObject.createWithoutData(
							Relation_ClassName,
							entity.getId()
						);
		}
		
		// add its general fields
		pRelation.put(Relation_AssistedPerson, fromUser(entity.getAssistedPerson(), false));
		pRelation.put(Relation_UnassistedPerson, fromUser(entity.getUnassistedPerson(), false));
		pRelation.put(Relation_Role, entity.getRole().toString());
		
		// return the generated parse object
		return pRelation;
	}
	private static ParseObject fromContactRequest(ContactRequest entity, boolean createNew) {
		ParseObject pContact;
		
		// create a new parse object with this class name & id
		if(createNew) {
			pContact = ParseObject.create(ContactRequest_ClassName);
		} else {
			pContact = ParseObject.createWithoutData(
							ContactRequest_ClassName,
							entity.getId()
						);
		}
		
		// add its sub type
		pContact.put(
							ContactRequest_Type,
							entity.getInitiator() instanceof AssistedPerson ? ContactRequest_Type_Request :
							entity.getInitiator() instanceof UnassistedPerson ? ContactRequest_Type_Offer :
																	null
					);
		
		// add all its general fields
		pContact.put(ContactRequest_Initiator, ParseUser.createWithoutData(ParseUser.class, entity.getInitiator().getId()));
		pContact.put(ContactRequest_RecipientEmail, entity.getRecipientEmail());
		pContact.put(ContactRequest_DesiredRole, entity.getDesiredRole().toString());
		pContact.put(ContactRequest_Responded, entity.getResponded());
		pContact.put(ContactRequest_Accepted, entity.getAccepted());
		
		// return the generated parse object
		return pContact;		
	}
	private static ParseObject fromCommunication(Communication communication, boolean createNew) {
		ParseObject pCommunication;
		
		// create a new ParseObject with this class name
		if(createNew) {
			pCommunication = ParseObject.create(Communication_ClassName);
		} else {
			pCommunication = ParseObject.createWithoutData(
							Communication_ClassName,
							communication.getId()
						);
		}
		
		// add its sub type
		pCommunication.put(
							Communication_Type,
							communication instanceof Message ?		Communication_Type_Message :
							communication instanceof Notification ?	Communication_Type_Notification :
																	null
						);
		
		// add all its general fields
		pCommunication.put(Communication_Sender, fromUser(communication.getSender(), false));
		pCommunication.put(Communication_Recipient, fromUser(communication.getRecipient(), false));
		pCommunication.put(Communication_Time, communication.getTime());
		pCommunication.put(Communication_Read, communication.getRead());
		
		// add the specific fields
		if(communication instanceof Message) {
			pCommunication.put(
						Communication_Message_Body,
						((Message) communication).getBody()
					);
		} else if(communication instanceof Notification) {
			pCommunication.put(
						Communication_Notification_Type,
						((Notification) communication).getType().toString()
					);
		}
		
		// return the generated ParseObject
		return pCommunication;
	}
	
	// ParseObject -> Entity
	
	public static Communication toCommunication(ParseObject pCommunication) {
		Communication c;
		
		// create a new type of Communication based on the given object's type field
		if(pCommunication.getString(Communication_Type).equals(Communication_Type_Message)) {
			c = new Message(pCommunication.getObjectId());
		} else if(pCommunication.getString(Communication_Type).equals(Communication_Type_Notification)) {
			c = new Notification(pCommunication.getObjectId());
		} else {
			// if the communication is neither type, this is invalid data
			return null;
		}
		
		// set the general fields
		c.setSender(toUser(pCommunication.getParseObject(Communication_Sender)));
		c.setRecipient(toUser(pCommunication.getParseObject(Communication_Recipient)));
		c.setTime(pCommunication.getDate(Communication_Time));
		c.setRead(pCommunication.getBoolean(Communication_Read));
		
		// set the specific fields
		if(c instanceof Message) {
			((Message) c).setBody(pCommunication.getString(Communication_Message_Body));
		} else if(c instanceof Notification) {
			((Notification) c).setType(NotificationType.valueOf(
						pCommunication.getString(Communication_Notification_Type)
					));
		}
		
		// return the generated Communication
		return c;
	}

	private static ContactRequest toContactRequest(ParseObject pRequest) {
		ContactRequest cr;
		
		// create a new type of request
		cr = new ContactRequest(pRequest.getObjectId());
		
		// set the general fields
		cr.setInitiator(toUser(pRequest.getParseObject(ContactRequest_Initiator)));
		cr.setRecipientEmail(pRequest.getString(ContactRequest_RecipientEmail));
		
		// if the contact request was an assistance request,
		if(pRequest.getString(ContactRequest_Type).equals(ContactRequest_Type_Request)) {
			// set the desired role
			cr.setDesiredRole(
						Role.fromString(pRequest.getString(ContactRequest_DesiredRole))
					);
		}
		
		// set the desired role
		cr.setDesiredRole(
					Role.fromString(pRequest.getString(ContactRequest_DesiredRole))
				);
		
		// set the accepted and responded boolean fields
		cr.setAccepted(
				pRequest.getBoolean(ContactRequest_Accepted)
		);
		
		cr.setResponded(
				pRequest.getBoolean(ContactRequest_Responded)
		);
		// return the generated ContactRequest
		return cr;
	}
	

	private static Relation toRelation(ParseObject pEntity) {
		Relation rel;
		
		// create a new relation
		rel = new Relation(pEntity.getObjectId());
		
		// set its fields
		rel.setAssistedPerson(toUser(pEntity.getParseObject(Relation_AssistedPerson)));
		rel.setUnassistedPerson(toUser(pEntity.getParseObject(Relation_UnassistedPerson)));
		rel.setRole(Role.fromString(pEntity.getString(Relation_Role)));
		
		// return the generated relation
		return rel;
	}
	
	private static User toUser(ParseObject pUser) {
		User u;

		// create a new instance of a User based on the given object's type field
		
		if (pUser.getBoolean(User_Assisted)) {
			u = new AssistedPerson(pUser.getObjectId());
		} else {
			u = new UnassistedPerson(pUser.getObjectId());
		}
		
		// set the general fields
		u.setFirstName(pUser.getString(User_FirstName));
		u.setLastName(pUser.getString(User_LastName));
		u.setEmail(pUser.getString(User_Email));
		u.setPhoneNumber(pUser.getString(User_PhoneNumber));
		
		// set the profile image if it exists
		ParseFile profileImage = pUser.getParseFile(User_ProfileImage);
		if (profileImage != null) {
			u.setProfileImage(profileImage.getUrl());
		}
		
		// if the user is an assisted person, set the extra fields
		if(u instanceof AssistedPerson) {
			AssistedPerson ap;
			// cast the user so we can do assisted person-y stuff
			ap = (AssistedPerson) u;
			// set the fields if they exist
			if(pUser.has(User_LCT)) {
				ap.setLastCommunicationTime(pUser.getDate(User_LCT));
			}
			if(pUser.has(User_LIT)) {
				ap.setLastInteractionTime(pUser.getDate(User_LIT));
			}
		}
		
		// return the generated User
		return u;
	}

}
