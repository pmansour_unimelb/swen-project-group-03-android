package au.org.intouch.framework.interfaces;

import au.org.intouch.framework.exceptions.InTouchException;

/**
 * This class is used to handle completion of an event that returns a result,
 * but can fail with a given exception.
 * 
 * @param T The type of the expected result.
 */
public abstract class ResultCallback<T> {
	public abstract void done(T result, InTouchException err);
}
