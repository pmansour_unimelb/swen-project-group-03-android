package au.org.intouch.framework.interfaces;

import java.util.List;

import au.org.intouch.framework.exceptions.InTouchException;
import au.org.intouch.models.Communication;
import au.org.intouch.models.Message;
import au.org.intouch.models.Notification;
import au.org.intouch.models.Notification.NotificationType;
import au.org.intouch.models.ContactRequest;
import au.org.intouch.models.Relation;
import au.org.intouch.models.Role;
import au.org.intouch.models.User;

/**
 * An interface that describes how this application expects data to be provided.
 * 
 * All of the interactions below (except getLoggedInUser) occur asynchronously,
 * and you are notified of the data being read or updated using a callback.
 * 
 * @author peter
 * @author anthony
 * @author stephen
 *
 */
public interface DataProvider {

	/**
	 * Get the data model for the logged in user.
	 * 
	 * Pre-condition: There is a logged in user.
	 */
	public User getLoggedInUser();
	/**
	 * Try to update the logged in user's details (besides email) to the new
	 * details in the given data model. Email is not updated from here.
	 * If you would like to update the logged in user's email, please use
	 * {@code updateEmail(String email, PasssiveCallback<InTouchException> callback)}.
	 * 
	 * @param newUserModel The data model that describes the new properties for the
	 * logged in user.
	 * @param callback The callback to use for finding out whether or not the
	 * task was successful. Throws a {@link InTouchException} if the update was
	 * unsuccessful. Can be null if you don't care.
	 */
	public void updateUserModel(User newUserModel, PassiveCallback callback);
	
	/**
	 * Try to change the logged in user's email to the given email.
	 * 
	 * @param email The new email that the user would like to have.
	 * @param callback The callback to use for finding out whether or not the
	 * task was successful. Throws a {@link InTouchException} if there was a problem
	 * updating to the given email. Can be null if you don't care.
	 */
	public void updateEmail(	String email, PassiveCallback callback);
	/**
	 * Try to change the logged in user's password to the given password.
	 * 
	 * @param oldPassword The logged in user's existing password.
	 * @param newPassword The new password that the user would like to have.
	 * @param callback The callback to use for finding out whether or not the
	 * task was successful. Throws a {@link InTouchException} if there was a problem
	 * updating to the given password. Can be null if you don't care.
	 */
	public void updatePassword(	String oldPassword, String newPassword,
								PassiveCallback callback);
	/**
	 * Try to change the logged in user's profile picture.
	 * 
	 * @param name The image filename. It's important to have an extension.
	 * @param imageData A byte array which contains the new image.
	 * @param callback Called when this is done.
	 */
	public void setProfilePicture(String name, byte[] imageData, PassiveCallback callback);

	/**
	 * Try to send a message from the logged in user to the given recipient
	 * with the given body.
	 * 
	 * @param recipient The message recipient.
	 * @param body The message body.
	 * @param callback The callback to use for finding out whether or not the
	 * task was successful. Throws a {@link InTouchException} if there was a
	 * problem in sending the message. Can be null if you don't care.
	 * @return 
	 */
	public Message sendMessage(	User recipient, String body,
								PassiveCallback callback);
	/**
	 * Try to send a notification from the logged in user to the given recipient
	 * with the given notification type.
	 * 
	 * @param recipient The notification recipient.
	 * @param type The notification type.
	 * @param callback The callback to use for finding out whether or not the
	 * task was successful. Throws a {@link InTouchException} if there was a
	 * problem in sending the notification. Can be null if you don't care.
	 */
	public void sendNotification(User recipient, NotificationType type,
								PassiveCallback callback);
	
	/**
	 * Try to set a given communication as read.
	 * 
	 * @param communication The notification that should be set as read.
	 * @param callback The callback to use for finding out whether or not the
	 * task was successful. Throws a {@link InTouchException} if there was a
	 * problem in sending the message. Can be null if you don't care.
	 */
	public void setCommunicationAsRead(Communication communication,
								PassiveCallback callback);
	
	/**
	 * Try to get all of the logged in user's unread communications.
	 * 
	 * @param callback The callback that gets called once the data is available
	 * or, in case an error occurred, a {@link InTouchException} is thrown.
	 */
	public void getCommunications(
								ResultCallback<List<Communication>> callback);
	
	/**
	 * Try to get all of the notifications where the current user is recipient.
	 * 
	 * @param callback
	 */
	public void getUnreadNotifications(ResultCallback<List<Notification>> callback);
	
	/**
	 * Try to get the communications that involve the logged in user and the
	 * given recipient, ordered by date. Only gets the ones between startIndex
	 * and endIndex (inclusive).
	 * 
	 * @param recipient The recipient that you want the conversations with.
	 * @param startIndex The index of the first communication to get.
	 * @param endIndex The index of the last communication to get.
	 * @param callback The callback that gets called once the data is available
	 * or, in case an error occurred, a {@link InTouchException} is thrown.
	 */
	public void getConversations(User recipient, int startIndex, int endIndex,
								ResultCallback<List<Communication>> callback);
	/**
	 * Try to get the communications that involve the logged in user and the
	 * given recipient, ordered by date. Only gets the {@literal count} soonest ones.
	 * 
	 * @param recipient The recipient that you want the conversations with.
	 * @param startIndex The index of the first communication to get.
	 * @param endIndex The index of the last communication to get.
	 * @param callback The callback that gets called once the data is available
	 * or, in case an error occurred, a {@link InTouchException} is thrown.
	 */
	public void getConversations(User recipient, int count,
								ResultCallback<List<Communication>> callback);
	/**
	 * Try to get all of the communications that involve the logged in user and
	 * the given recipient, ordered by date. Gets all the communications.
	 * 
	 * @param recipient The recipient that you want the conversations with.
	 * @param startIndex The index of the first communication to get.
	 * @param endIndex The index of the last communication to get.
	 * @param callback The callback that gets called once the data is available
	 * or, in case an error occurred, a {@link InTouchException} is thrown.
	 */
	public void getConversations(User recipient,
								ResultCallback<List<Communication>> callback);

	/**
	 * Try to get all of the logged in user's contacts.
	 * 
	 * @param callback The callback that gets called when the data is available
	 * or, in case an error occurred, when a {@link InTouchException} is thrown.
	 */
	public void getContacts(
							ResultCallback<List<Relation>> callback);
	
	/**
	 * Try to send a contact request to another user with the given
	 * email.
	 * 
	 * @param recipientEmail The email of the other user who should
	 * receive this request.
	 * @param desiredRole The desired role of this request.
	 * @param callback The callback to use for finding out whether or not the
	 * task was successful. Throws a {@link InTouchException} if the request was
	 * not sent successfully. Can be null if you don't care.
	 */
	public void sendContactRequest(String recipientEmail, Role desiredRole,
						PassiveCallback callback);
	
	/**
	 * Try to remove the given user from the logged in user's contact list.
	 * @param contact The User to remove.
	 * @param callback
	 */
	public void removeContact(User contact, PassiveCallback callback);
	
	/**
	 * Update an assistance offer
	 * (Either ignore or deny, depending on the internal state of the object)
	 * @param request
	 * @param callback
	 */
	public void updateContactRequest(ContactRequest request, PassiveCallback callback);
	
	/**
	 * Try to get the logged in user's pending assistance offers.
	 * 
	 * @param callback The callback that gets called when the data is available
	 * or, in case an error occurred, when a {@link InTouchException} is thrown.
	 */
	public void getContactRequests(ResultCallback<List<ContactRequest>> callback);
	
	public void getNotificationById(String id, ResultCallback<Notification> callback);
}
