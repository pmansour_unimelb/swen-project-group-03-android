package au.org.intouch.framework.interfaces;

/**
 * A special type of DataProvider for when the logged in user is an
 * UnassistedPerson. Provides extra functionality that only an UnassistedPerson
 * can do.
 * 
 * @author peter
 * @author anthony
 * @author stephen
 *
 */
public interface UnassistedDataProvider extends DataProvider {

}