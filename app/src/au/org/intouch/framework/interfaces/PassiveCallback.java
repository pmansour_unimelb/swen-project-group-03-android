package au.org.intouch.framework.interfaces;

import au.org.intouch.framework.exceptions.InTouchException;

/**
 * This class is used to handle completion of an event that does not return
 * anything, but can raise a given exception.
 * @author peter
 */
public abstract class PassiveCallback {
	public abstract void done(boolean successful, InTouchException err);
}
