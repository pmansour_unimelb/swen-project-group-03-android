package au.org.intouch.framework.interfaces;

import java.util.List;

import au.org.intouch.framework.exceptions.InTouchException;
import au.org.intouch.models.UnassistedPerson;

/**
 * A special type of DataProvider for when the logged in user is an
 * AssistedPerson. Provides extra functionality that only an AssistedPerson
 * can do.
 * 
 * @author peter
 * @author stephen
 * @author anthony
 *
 */
public interface AssistedDataProvider extends DataProvider {
	
	/**
	 * Try to get the logged in user's primary carer.
	 * 
	 * Warning: this can take a few seconds, or even fail if the cache is empty
	 * and there is no internet connection available.
	 * 
	 * @param callback The callback that gets called when the data is available
	 * or, in case an error occurred, when a {@link InTouchException} is thrown.
	 */
	public void getPrimaryCarer(ResultCallback<UnassistedPerson> callback);
	/**
	 * Try to get the logged in user's list of carers.
	 * 
	 * @param callback The callback that gets called when the data is available
	 * or, in case an error occurred, when a {@link InTouchException} is thrown.
	 */
	public void getCarers(ResultCallback<List<UnassistedPerson>> callback);
	/**
	 * Try to get the logged in user's list of friends.
	 * 
	 * @param callback The callback that gets called when the data is available
	 * or, in case an error occurred, when a {@link InTouchException} is thrown.
	 */
	public void getFriends(ResultCallback<List<UnassistedPerson>> callback);


	/**
	 * Try to update the user's last communication time.
	 * 
	 * @param callback The callback to use for finding out whether or not the
	 * task was successful. Throws a {@link InTouchException} if the update was
	 * unsuccessful. Can be null if you don't care.
	 */
	public void updateLastCommunicationTime(PassiveCallback callback);
	/**
	 * Try to update the user's last interaction time.
	 * 
	 * @param callback The callback to use for finding out whether or not the
	 * task was successful. Throws a {@link InTouchException} if the update was
	 * unsuccessful. Can be null if you don't care.
	 */
	public void updateLastInteractionTime(PassiveCallback callback);
	
}