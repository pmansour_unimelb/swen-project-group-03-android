package au.org.intouch.framework.interfaces;

import au.org.intouch.framework.exceptions.InTouchException;

/**
 * An interface that describes how this application expects authentication to
 * be done.
 * 
 * @author peter
 * @author stephen
 * @author anthony
 *
 */
public interface AuthenticationProvider {
	public static enum UserType { AssistedPerson, UnassistedPerson };

	/**
	 * Try to register a new user to the system with the given details.
	 * @param email The email that the new user would like to sign up with.
	 * @param password The password that the new user would like.
	 * @param firstName The user's first name.
	 * @param lastName The user's last name.
	 * @param phoneNumber The user's phone number.
	 * @param userType The type of user to register.
	 * 
	 * @param callback The callback to use for specifying whether or not the
	 * task was successful. Throws an {@link InTouchException} if registration
	 * was unsuccessful. Can be null if you don't care.
	 */
	public void register(	String email, String password,
							String firstName, String lastName,
							String phoneNumber, UserType userType,
							PassiveCallback callback);
	/**
	 * Try to reset the user with the given email's password.
	 * 
	 * @param email The email of the user who wants to reset their password.
	 * @param callback The callback to use for specifying whether or not the
	 * task was successful. Throws an {@link InTouchException} if there
	 * was a problem during the attempt. Can be null if you don't care.
	 */
	public void requestPasswordReset(String email,
						PassiveCallback callback);
	
	/**
	 * Try to log an existing user in with the given username and password.
	 * @param email The email of the user.
	 * @param password The password of the user.
	 * 
	 * @param callback The callback to use for specifying whether or not the
	 * task was successful. Throws an {@link InTouchException} if the login
	 * was unsuccessful, such as when the credentials are incorrect. Can be null
	 * if you don't care.
	 */
	public void login(	String email, String password,
						PassiveCallback callback);
	/**
	 * Try to log the currently logged in user out.
	 * 
	 * @param callback The callback to use for specifying whether or not the
	 * task was successful. Throws an {@link InTouchException} if there
	 * was a problem during the logout attempt. Can be null if you don't care.
	 */
	public void logout(PassiveCallback callback);
	/**
	 * Find out whether or not there is a currently logged in user.
	 * @return true if there's a logged in user, false otherwise.
	 */
	public boolean isLoggedIn();
	
	/**
	 * Get the data interface implementation associated with the logged in user.
	 * This can either be an implementation of AssistedDataInterface or
	 * UnassistedDataInterface, depending on the type of logged in user.
	 * 
	 * If there is no logged in user, then this method should return null.
	 */
	public DataProvider getProvider();
	
}
