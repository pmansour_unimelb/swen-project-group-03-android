package au.org.intouch.framework.exceptions;

import android.content.Context;
import au.org.intouch.R;

/**
 * The possible problems that could have happened to cause an exception.
 * @author peter
 */
public enum Problem {
	// general problems
	InvalidEmail(R.string.Problem_InvalidEmail),
	InvalidPassword(R.string.Problem_InvalidPassword),
	ExistingEmail(R.string.Problem_ExistingEmail),
	NoNetwork(R.string.Problem_NoNetwork),
	
	// registration problems
	RegistrationOther(R.string.Problem_RegistrationOther),
	// login/logout problems
	LoginIncorrectCredentials(R.string.Problem_IncorrectCredentials),
	LoginOther(R.string.Problem_LoginOther),
	LogoutOther(R.string.Problem_LogoutOther),
	// request password reset problems
	RequestPasswordResetOther(R.string.Problem_RequestPasswordResetOther),
	
	// data loading/saving problems	
	SaveOther(R.string.Problem_SaveOther),
	LoadOther(R.string.Problem_LoadOther),
	
	// EntityAdapter problems
	EntityAdapterInvalidData(R.string.Problem_InvalidData),
	
	FetchOther(R.string.Problem_FetchOther),
	
	// Cache problems
	CacheMiss(R.string.Problem_CacheMiss),
	
	// unexpected problems
	Unexpected(R.string.Problem_Unexpected);
	
	/** The id of the string that describes this problem. */
	private int _messageResId;
	
	// constructor
	
	private Problem(int messageResId) {
		_messageResId = messageResId;
	}
	
	// helper method
	
	public String toString(Context context) {
		return context.getString(_messageResId);
	}
}
