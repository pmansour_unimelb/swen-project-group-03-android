package au.org.intouch.framework.exceptions;

import android.content.Context;
import android.widget.Toast;

public class InTouchException extends Exception {
	private static final long serialVersionUID = 3361704168043188534L;
	
	/** how long a toast should be shown for (in seconds). */
	public static final int TOAST_DURATION = 10;

	// private instance variable

	private Problem _cause;
	private Exception _underlyingException;

	/**
	 * Create a new InTouchException with the given cause and the underlying
	 * Exception that caused it.
	 * @param problem The problem that may have caused this exception.
	 * @param underlyingException The exception that caused this exception.
	 */
	public InTouchException(Problem cause, Exception underlyingException) {
		super();

		// hang on to the given information
		_cause = cause;
		_underlyingException = underlyingException;
	}
	/**
	 * Create a new InTouchException with the given cause and no underlying
	 * exception.
	 * @param problem The problem that may have caused this exception.
	 */
	public InTouchException(Problem cause) {
		this(cause, null);
	}

	/**
	 * Get the problem that may have caused this exception.
	 */
	public Problem getProblem() {
		return _cause;
	}
	/**
	 * Get the exception that may have caused this exception.
	 */
	public Exception getUnderlyingException() {
		return _underlyingException;
	}

	/**
	 * Get the string message associated with the cause of this exception.
	 * @param context The context to use to get the string of the message.
	 */
	public String toString(Context context) {
		return _cause.toString(context);
	}
	
	/**
	 * Is this exception important enough to show to the user?
	 */
	public boolean isImportant() {
		// it's important if it's not a cache problem
		return _cause.equals(Problem.CacheMiss) == false;
	}
	
	/**
	 * Deal with this exception - I don't care how!
	 * 
	 * If the exception is not important, it will do nothing.
	 * Otherwise, it will show a toast.
	 */
	public void dealWithIt(Context context) {
		// if the context is invalid, do nothing
		if(context == null) {
			return;
		}
		
		// if the exception is important,
		if(isImportant()) {
			// print the stack trace (if there's an underlying exception)
			if(_underlyingException != null) {
				_underlyingException.printStackTrace(System.err);
			}
			// show a toast
			Toast.makeText(context, toString(context), Toast.LENGTH_LONG).show();
		}
	}
}
