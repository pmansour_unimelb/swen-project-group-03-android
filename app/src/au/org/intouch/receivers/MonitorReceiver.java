package au.org.intouch.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.util.Log;
import au.org.intouch.InTouch;
import au.org.intouch.framework.interfaces.AuthenticationProvider.UserType;

public class MonitorReceiver extends BroadcastReceiver {
	
	/**
	 * We only care about these broadcasts if
	 * 	(1) There is a logged in user.
	 * 	(2) The logged in user is an AssistedPerson.
	 */
	private boolean doWeCare() {
		return
				InTouch.isLoggedIn() &&
				InTouch.getLoggedInUserType() == UserType.AssistedPerson
			;
	}

	public void onContacted() {
		// if we care,
		if(doWeCare()) {
			// update the last communication time
			InTouch.getAssistedDataProvider().updateLastCommunicationTime(null);
		}
	}
	
	public void onDeviceUsed(){
		// if we care,
		if(doWeCare()) {
			// update the last interaction time
			InTouch.getAssistedDataProvider().updateLastInteractionTime(null);
		}
	}
	
	@Override
	public void onReceive(Context content, Intent intent) {
		// log this intent
		Log.i("Received intent", intent.toString());
		Log.i("Received intent with action:", intent.getAction());
		
		// if the user is in a call
		if (
				// the phone state was changed (phone state is active),
				intent.getAction().equals(TelephonyManager.ACTION_PHONE_STATE_CHANGED) &&
				// and it's a call
				intent.getStringExtra(TelephonyManager.EXTRA_STATE)
					.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)
			) {
			// then the user has had contact with someone
			onContacted();
			// log this
			Log.i("Phone", "intent is offhook");
		// just generally, if the user has unlocked the screen
		} else if (intent.getAction().equals(Intent.ACTION_USER_PRESENT)){
			// then the device has been used
			onDeviceUsed();
		}
	}
}
