package au.org.intouch.receivers;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import au.org.intouch.InTouch;
import au.org.intouch.framework.exceptions.InTouchException;
import au.org.intouch.framework.interfaces.ResultCallback;
import au.org.intouch.logic.InTouchNotificationManager;
import au.org.intouch.models.Notification;

public class PushReceiver extends BroadcastReceiver {
	public static final String INTOUCH_PUSH_ACTION = "au.org.intouch.push_notification";
	public static final String DATA = "com.parse.Data";
	public static final String DATA_NOTIFICATION_ID = "notificationId";
	
	@Override
	public void onReceive(final Context context, Intent intent) {
		JSONObject data;
		String notificationId;
		
		// make sure we got the right broadcast
		if(intent.getAction().equals(INTOUCH_PUSH_ACTION) == false) {
			return;
		}
		
		try {
			// get the "data" JSON object
			data = new JSONObject(intent.getExtras().getString(DATA));
			// get the notification id from it
			notificationId = data.getString(DATA_NOTIFICATION_ID);
		} catch (JSONException e) {
			// print the stack trace
			e.printStackTrace(System.err);
			// exit
			return;
		}
		
		// get the notification with that id
		InTouch.getProvider().getNotificationById(notificationId, new ResultCallback<Notification>() {
			@Override
			public void done(Notification result, InTouchException err) {
				// if there was a problem, deal with it and exit
				if(err != null) {
					err.dealWithIt(context);
					return;
				}
				
				// show the notification
				new InTouchNotificationManager(context).showNotification(result);
			}
		});
	}
}
