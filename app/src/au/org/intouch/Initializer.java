package au.org.intouch;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * Put all the initialization stuff here.
 * 
 * Some of the libraries and packages that we use need to be initialized when
 * the application is created. This was all done in the InTouch.java class, and
 * it really cluttered it up as a result.
 * 
 * This class is meant to host all of the initialization code blocks, grouped
 * in methods. The initAll() method just calls all of these methods (think of
 * it like make all in a Makefile).
 * 
 * This approach will help us know where to add more initialization blocks later,
 * and will make it a lot easier to find a specific initialization block that
 * we might want to edit later.
 * 
 * @author peter
 *
 */
public class Initializer {

	private InTouch app;
	
	public Initializer(InTouch app) {
		this.app = app;
	}
	
	public void initAll() {
		initImageLoader();
	}
	
	public void initImageLoader() {
		DisplayImageOptions defaultOptions;
		ImageLoaderConfiguration config;
		
		// default options
		defaultOptions = new DisplayImageOptions.Builder()
        	.cacheInMemory(true)
        	.cacheOnDisc(true)
        	.build();
		
        // Create global configuration and initialize ImageLoader with this configuration
        config = new ImageLoaderConfiguration.Builder(app.getApplicationContext())
        	.defaultDisplayImageOptions(defaultOptions)
        	.build();
        
        // initialize the image loader
        ImageLoader.getInstance().init(config);
	}
}
