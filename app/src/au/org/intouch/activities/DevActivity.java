package au.org.intouch.activities;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import au.org.intouch.R;

public class DevActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dev);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.dev, menu);
		return true;
	}

}
