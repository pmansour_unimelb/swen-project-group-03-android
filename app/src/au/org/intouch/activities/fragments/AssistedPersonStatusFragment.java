package au.org.intouch.activities.fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * This fragment will be part of the ProfileActivity activity, and will show
 * things relating to the status of AssistedPerson users.
 * The things to be shown are:
 * 	-> The last communication time (LCT) of the user
 * 	-> The last interaction time (LIT) of the user
 * 
 * @author peter and anthony
 *
 */
public class AssistedPersonStatusFragment extends StatusFragment {

	@Override
	protected View getBodyView(LayoutInflater inflater, ViewGroup container) {
		// TODO Auto-generated method stub
		return null;
	}

	
}
