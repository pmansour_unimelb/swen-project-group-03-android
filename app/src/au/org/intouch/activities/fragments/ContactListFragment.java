package au.org.intouch.activities.fragments;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Toast;
import au.org.intouch.InTouch;
import au.org.intouch.R;

import au.org.intouch.activities.ContactAddActivity;
import au.org.intouch.activities.ProfileActivity;
import au.org.intouch.activities.fragments.dividers.DividedArrayAdapter;
import au.org.intouch.framework.exceptions.InTouchException;
import au.org.intouch.framework.interfaces.AuthenticationProvider.UserType;
import au.org.intouch.framework.interfaces.ResultCallback;
import au.org.intouch.models.Relation;
import au.org.intouch.models.Role;
import au.org.intouch.models.User;

import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * @author anthony
 *
 */
public class ContactListFragment extends Fragment implements OnItemClickListener {
	protected ImageLoader imageLoader = ImageLoader.getInstance();

	private ListView list;
	private ContactAdapter adapter;

	//private ArrayList<Conversation> arr = new ArrayList<Conversation>();
	
	public ContactListFragment() {}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView   = inflater.inflate(R.layout.fragment_contactlist, container, false);

		// when the user clicks "add contact",
		rootView.findViewById(R.id.add_contact).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent;
				
				// create an intent to go to the "add contact" activity
				intent = new Intent(getActivity(), ContactAddActivity.class);
				// go there
				startActivity(intent);
			}
		});
		
		// find the list view in the layout
		list = (ListView)  rootView.findViewById(R.id.list);
		
		// add the user's contacts
		updateContacts();
		
		// handle click events in the list
		list.setOnItemClickListener(this);

		return rootView;
	}
	
	private void updateContacts() {
		InTouch.getProvider().getContacts(new ResultCallback<List<Relation>>() {
			@Override
			public void done(List<Relation> relations, InTouchException err) {
				if (getActivity() == null) {
					return;
				}
				// if there was a problem,
				if(err != null) {
					// print the stack trace for any connected debuggers
					err.getUnderlyingException().printStackTrace(System.err);
					// show a toast
					Toast.makeText(getActivity(), err.toString(getActivity()), Toast.LENGTH_LONG).show();
					// exit
					return;
				}
				
				// otherwise,
				// update the list adapter
				adapter = new ContactAdapter(getActivity(), relations, InTouch.getLoggedInUserType());
				list.setAdapter(adapter);
			}
		});
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Intent intent;
		
		// if the item at that position is a divider, ignore it
		if(adapter.getItem(position).isDivider()) {
			return;
		}
		
		// otherwise, open the profile of that user
		intent = new Intent(getActivity(), ProfileActivity.class);
		intent.putExtra(ProfileActivity.EXTRA_USER, adapter.getUser(position));
		startActivity(intent);
	}

	/**
	 * List view adapter for the list of relations that the user is involved in.
	 */
	private class ContactAdapter extends DividedArrayAdapter<Relation, Role> {

		private UserType _loggedInUserType;
		
		public ContactAdapter(Context context, List<Relation> arr, UserType loggedInUserType) {
			super(context, R.layout.contact_list_item, arr);
			
			// set the logged in user type
			this._loggedInUserType = loggedInUserType;
		}
		
		public User getUser(int position) {
			Relation relation;
			
			// if that position has a divider, return null
			if(getItem(position).isDivider()) {
				return null;
			}
			
			// get the relation at that position
			relation = getItem(position).getItemValue();
			
			// return the correct user in that relation
			return	_loggedInUserType == UserType.AssistedPerson ?
						relation.getUnassistedPerson() :
							relation.getAssistedPerson();
		}

		@Override
		protected Role getType(Relation object) {
			return object.getRole();
		}

		@Override
		protected View getDividerView(Role type, View convertView, ViewGroup parent) {
			TextView text;
			
			// inflate a new view for the divider
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.divider, parent, false);
			
			// get the text view in this layout
			text = (TextView) convertView.findViewById(R.id.text);
			
			// set the text to the role name
			if(_loggedInUserType == UserType.AssistedPerson) {
				text.setText(type.toString() + "s");
			} else {
				text.setText("People for whom you are a " + type.toString());
			}

			// return that view
			return convertView;
		}

		@Override
		protected View getValueView(Relation value, View convertView, ViewGroup parent) {
			ImageView image;
			TextView name;
			User otherUser;
			
			// inflate a new view for the contact
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.contact_list_item, parent, false);
			
			// find the views to fill
			image = (ImageView) convertView.findViewById(R.id.image);
			name = (TextView) convertView.findViewById(R.id.name);
			
			// find the user we need to show
			otherUser = _loggedInUserType == UserType.AssistedPerson ?
							value.getUnassistedPerson() :
								value.getAssistedPerson();
			
			// fill the image and name
			imageLoader.displayImage(
								otherUser.getProfileImage(),
								image
							);
			name.setText(otherUser.getFullName());
			
			// return that view
			return convertView;
		}
	}
	
}