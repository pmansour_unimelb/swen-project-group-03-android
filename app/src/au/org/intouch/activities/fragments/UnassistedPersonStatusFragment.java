package au.org.intouch.activities.fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * This fragment will be part of the ProfileActivity activity, and will show
 * things relating to the status of UnassistedPerson users.
 * The things to be shown are:
 * 	-> The role of that user in his/her relationship with the logged in user.
 * 
 * @author peter and anthony
 *
 */
public class UnassistedPersonStatusFragment extends StatusFragment {

	@Override
	protected View getBodyView(LayoutInflater inflater, ViewGroup container) {
		// TODO Auto-generated method stub
		return null;
	}

}
