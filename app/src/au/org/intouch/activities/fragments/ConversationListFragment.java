package au.org.intouch.activities.fragments;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import au.org.intouch.InTouch;
import au.org.intouch.R;
import au.org.intouch.activities.ProfileActivity;
import au.org.intouch.framework.exceptions.InTouchException;
import au.org.intouch.framework.interfaces.ResultCallback;
import au.org.intouch.models.Communication;
import au.org.intouch.models.User;
import au.org.intouch.models.Conversation;

import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * The "Status" tab on the profile view to show details about an assited person (.e.g, last contact time)
 * Visible to carers.
 * 
 *  This is reached after a user taps the assisted person in the contact list,
 *  or (by default) simply opens the app and has already logged in.
 * 
 * @author anthony
 *
 */
public class ConversationListFragment extends Fragment implements OnItemClickListener {
	protected ImageLoader imageLoader = ImageLoader.getInstance();

	private ListView list;
	User person;
	
	public ConversationListFragment() {}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView   = inflater.inflate(R.layout.fragment_home_conversations, container, false);

		list = (ListView)  rootView.findViewById(R.id.list);
		
		// This would ideally be done in XML, but it seems there is a bug with that.
		list.setDivider(null);
		list.setDividerHeight(0);
		
		list.setOnItemClickListener(this);

		// get all the user's contacts
		InTouch.getProvider().getCommunications(new ResultCallback<List<Communication>>() {

			@Override
			public void done(List<Communication> messages, InTouchException err) {
				List<Conversation> conversations;
				
				if (getActivity() == null){
					return;
				}
				
				// if there was an error,
				if(err != null) {
					// deal with it
					err.dealWithIt(getActivity());
					// exit this method
					return;
				}
				
				// group the communications by conversation
				conversations  = Conversation.collate(
											InTouch.getProvider().getLoggedInUser(),
											messages
										);
				
				// create a new adapter with them and use it
				
				list.setAdapter(new ConversationAdapter(getActivity(), conversations));

			}
			
		});
		
		return rootView;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Conversation c =  (Conversation) list.getAdapter().getItem(position);
		if (c == null) return;
		User u = c.getUser();
		
		Intent intent = new Intent(getActivity(), ProfileActivity.class);
		intent.putExtra(
			ProfileActivity.EXTRA_USER,
			u
		);
		
		// if they tapped the number
		intent.putExtra(ProfileActivity.EXTRA_TAB, ProfileActivity.TAB_MESSAGES);
		
		startActivity(intent);
	}

	/**
	 * List view adapter for the list of monitoring services the profile has enabled
	 *
	 * E.g., there may be two: one for monitoring the last time the phone was used, and another for
	 *       the last time the phone was used to make a call.
	 */
	private class ConversationAdapter extends ArrayAdapter<Conversation> {

		public ConversationAdapter(Context context, List<Conversation> arr) {
			super(context, R.layout.conversation_item, arr);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.conversation_item, null);
			}

			Conversation c = getItem(position);
			
			User u = c.getUser();
			
			ImageView image = (ImageView) convertView.findViewById(R.id.image);
			
			TextView nameLabel  = (TextView)  convertView.findViewById(R.id.name);
			
			nameLabel.setText(u.getFullName());
			
			TextView countLabel  = (TextView)  convertView.findViewById(R.id.count);
			
			countLabel.setText(c.getCommunications().size() + "");
			
			imageLoader.displayImage(u.getProfileImage(), image);
			
			return convertView;

		}


	}
	
}