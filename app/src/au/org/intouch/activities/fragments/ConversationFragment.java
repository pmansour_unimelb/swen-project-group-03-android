package au.org.intouch.activities.fragments;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import au.org.intouch.R;
import au.org.intouch.activities.ProfileActivity;
import au.org.intouch.framework.interfaces.PassiveCallback;
import au.org.intouch.framework.interfaces.ResultCallback;
import au.org.intouch.models.Communication;
import au.org.intouch.models.Message;
import au.org.intouch.models.Notification;
import au.org.intouch.models.User;
import au.org.intouch.InTouch;
import au.org.intouch.framework.exceptions.InTouchException;

import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * @author anthony
 *
 */
public class ConversationFragment extends Fragment implements OnItemClickListener {
	/** User: The user with whom the conversation should be shown. */
	public static final String EXTRA_WITHWHO = "WithWho";

	// instance variables

	private User _withWho;

	private EditText messageView;


	protected ImageLoader imageLoader = ImageLoader.getInstance();

	private ListView list;
	private ArrayAdapter<Communication> adapter;
	private ArrayList<Communication> messages = new ArrayList<Communication>();

	public ConversationFragment() {}

	private void refresh() {
		if (getActivity() == null) {
			return;
		}

		_withWho = (User) getActivity().getIntent().getSerializableExtra(ProfileActivity.EXTRA_USER);

		Log.e("Refresh", "messages " + _withWho.getId());
		InTouch.getProvider().getConversations(_withWho, new ResultCallback<List<Communication>>() {
			@Override
			public void done(List<Communication> communications, InTouchException exception) {
				// if it wasn't successful, deal with it
				if(exception != null) {
					exception.dealWithIt(getActivity());
					return;
				}
				messages.clear();
				messages.addAll(communications);

				adapter.notifyDataSetChanged();

				scrollToBottom(false);

				Log.e("Refresh", "response");
			}
		});
	}



	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView   = inflater.inflate(R.layout.fragment_conversation, container, false);

		Intent i;

		messages.clear();

		// load the list
		list = (ListView)  rootView.findViewById(android.R.id.list);
		list.setOnItemClickListener(this);
		list.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
		list.setStackFromBottom(true);
		list.setAdapter(adapter = new CommunicationAdapter(getActivity(), messages));
		scrollToBottom(true);

		// get the user with whom the conversation should be shown
		i = getActivity().getIntent();
		_withWho = (User) i.getSerializableExtra(ProfileActivity.EXTRA_USER);


		messageView = (EditText) rootView.findViewById(R.id.message);
		messageView
		.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
				if (id == R.id.send || id == EditorInfo.IME_NULL) {
					sendMessage();
					return true;
				}
				return false;
			}

		});

		InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(messageView.getWindowToken(), 0);


		rootView.findViewById(R.id.sendButton).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View view) {
						sendMessage();
					}
				}
				);


		// get the actual conversations (asynchronously)
		refresh();

		return rootView;
	}

	private void scrollToBottom(boolean force) {
	}

	private void sendMessage() {
		String message = messageView.getText().toString();
		if (message.length() == 0) return;

		Message m = InTouch.getProvider().sendMessage(_withWho, message, new PassiveCallback(){

			@Override
			public void done(boolean successful, InTouchException err) {
				refresh();
			}
		});

		// immediately add it to the list
		messages.add(m);

		adapter.notifyDataSetChanged();
		scrollToBottom(true);

		messageView.setText("");


	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {


	}

	/**
	 * List view adapter for the list of monitoring services the profile has enabled
	 *
	 * E.g., there may be two: one for monitoring the last time the phone was used, and another for
	 *       the last time the phone was used to make a call.
	 */
	private class CommunicationAdapter extends ArrayAdapter<Communication> {

		public CommunicationAdapter(Context context, List<Communication> arr) {
			super(context, R.layout.contact_list_item, arr);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.message_list_item, null);
			}

			Communication c = getItem(position);
			TextView body  = (TextView) convertView.findViewById(R.id.body);

			if(c instanceof Message) {
				Message m = (Message) c;
				body.setText(m.getBody());
			} else {
				Notification n = (Notification) c;
				
				switch(n.getType()) {
					case RED:
						body.setText(getActivity().getString(R.string.notification_red_notification_body, n.getSender().getFullName()));
						break;
					case YELLOW:
						body.setText(getActivity().getString(R.string.notification_yellow_notification_body, n.getSender().getFullName()));
						break;
					default:
						body.setText("");
				}
			}

			return convertView;

		}


	}

}