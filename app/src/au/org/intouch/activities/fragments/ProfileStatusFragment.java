package au.org.intouch.activities.fragments;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import au.org.intouch.R;
import au.org.intouch.activities.ProfileActivity;
import au.org.intouch.models.AssistedPerson;
import au.org.intouch.models.User;

import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * The "Status" tab on the profile view to show details about an assited person (.e.g, last contact time)
 * Visible to carers.
 * 
 *  This is reached after a user taps the assisted person in the contact list,
 *  or (by default) simply opens the app and has already logged in.
 * 
 * @author anthony
 *
 */
public class ProfileStatusFragment extends Fragment implements OnItemClickListener {
	protected ImageLoader imageLoader = ImageLoader.getInstance();

	private ListView  monitorList;
	private ImageView profileImage;
	User person;

	private ArrayList<Monitor> arr = new ArrayList<Monitor>();
	
	public ProfileStatusFragment() {}
	
	public void startCall() {
		String uri = "tel:" + person.getPhoneNumber();
		Intent i = new Intent(Intent.ACTION_CALL);
		i.setData(Uri.parse(uri));
		startActivity(i);
	}
	
	/**
	 * Monitor is a class that just stores 3 bits of information:
	 * label, time and an icon android image resource.
	 * 
	 * It provides the ability to generate strings like "3 minutes ago"
	 * 
	 * @author anthony
	 *
	 */
	private class Monitor {
		String _label;
		Date _time;
		int _img;
		
		public Monitor(String label, Date time, int img) {
			_label = label;
			_time = time;
			_img = img;
		}
		public String getLabel() {
			return _label;
		}
		
		/**
		 * If the time is close enough, will be written as something like "2 hours ago"
		 * 
		 * Otherwise, it will show a date.
		 * 
		 * @return String
		 */
		public String getStatusString() {
			if (_time == null) {
				return "";
			}
			return (String) DateUtils.getRelativeTimeSpanString(_time.getTime(), new Date().getTime(), DateUtils.MINUTE_IN_MILLIS, DateUtils.FORMAT_ABBREV_RELATIVE);
		}
		/**
		 * The icon
		 * 
		 * @return android image resource, e.g. R.drawable.someicon
		 */
		public int getImageResource() {
			return _img;
		}
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView   = inflater.inflate(R.layout.fragment_profile_status, container, false);
		View headerView = inflater.inflate(R.layout.profile_header, null);

		final ProfileActivity activity = (ProfileActivity) getActivity();
		
		person = (User) activity.getIntent().getSerializableExtra(ProfileActivity.EXTRA_USER);
		
		monitorList  = (ListView)  rootView.findViewById(R.id.monitorList);
		profileImage = (ImageView) headerView.findViewById(R.id.profileImage);

		View buttons = (View) headerView.findViewById(R.id.buttons);
		
		// We don't want the user to tap the send messages button or start a phone call with themselves
		// So hide those buttons:
		if (activity.isViewingMyself()) {
			buttons.setVisibility(View.GONE);
		} else {
			buttons.setVisibility(View.VISIBLE);
		}

		Button messageButton = (Button) headerView.findViewById(R.id.sendMessage);
		Button callButton	 = (Button) headerView.findViewById(R.id.call);

		callButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startCall();
			}
		});

		messageButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				
				// open the messages tab with the keyboard open.
				// usually the keyboard would not be open by default.
				ProfileActivity activity = (ProfileActivity) getActivity();
				activity.selectTab(ProfileActivity.TAB_MESSAGES);
			}
		});

		
		// Generate the data fields to show for this person:
		
		if (person instanceof AssistedPerson) {
			AssistedPerson ap = (AssistedPerson) person;
			
			arr.add(new Monitor("Device Last Used", ap.getLastInteractionTime(), R.drawable.phone));
			arr.add(new Monitor("Last communicated", ap.getLastCommunicationTime(), R.drawable.lastcall));
			
		}

		monitorList.addHeaderView(headerView);
		monitorList.setOnItemClickListener(this);

		if(savedInstanceState == null){
			MonitorAdapter adapter = new MonitorAdapter(getActivity(), arr);
			monitorList.setAdapter(adapter);
		}
		
		imageLoader.displayImage(person.getProfileImage(), profileImage);
		
		return rootView;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * List view adapter for the list of monitoring services the profile has enabled
	 *
	 * E.g., there may be two: one for monitoring the last time the phone was used, and another for
	 *       the last time the phone was used to make a call.
	 */
	private static class MonitorAdapter extends ArrayAdapter<Monitor> {

		public MonitorAdapter(Context context, List<Monitor> arr) {
			super(context, R.layout.profile_monitor_list_item, arr);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			if(convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.profile_monitor_list_item, null);
			}

			Monitor mon = getItem(position);
			
			TextView label  = (TextView)  convertView.findViewById(R.id.label);
			TextView value  = (TextView)  convertView.findViewById(R.id.value);
			ImageView image = (ImageView) convertView.findViewById(R.id.image);
			
			label.setText(mon.getLabel());
			value.setText(mon.getStatusString());

			image.setImageResource(mon.getImageResource());
			
			return convertView;

		}

	}
	
}