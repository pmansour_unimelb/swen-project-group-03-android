package au.org.intouch.activities.fragments.dividers;

/**
 * An item of a divided array adapter, which can either be a type divider or a
 * real item.
 * @author peter
 *
 * @param <T> The java type of real items.
 * @param <U> The java type of item types.
 */
public class Item<T, U extends Comparable<U>> {
	private boolean isDivider;
	private T itemValue;
	private U typeValue;
	
	public Item(T item) {
		isDivider = false;
		itemValue = item;
	}
	public Item(U type) {
		isDivider = true;
		typeValue = type;
	}
	
	public boolean isDivider() {
		return isDivider;
	}
	public T getItemValue() {
		if(!isDivider) {
			return itemValue;
		} else {
			return null;
		}
	}
	public U getTypeValue() {
		if(isDivider) {
			return typeValue;
		} else {
			return null;
		}
	}
}
