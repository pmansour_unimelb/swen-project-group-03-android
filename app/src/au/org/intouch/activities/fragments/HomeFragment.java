package au.org.intouch.activities.fragments;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import au.org.intouch.AssistanceAppWidgetProvider;
import au.org.intouch.InTouch;
import au.org.intouch.R;
import au.org.intouch.activities.ContactAddActivity;
import au.org.intouch.activities.MainActivity;
import au.org.intouch.activities.ProfileActivity;
import au.org.intouch.framework.exceptions.InTouchException;
import au.org.intouch.framework.interfaces.AuthenticationProvider.UserType;
import au.org.intouch.framework.interfaces.PassiveCallback;
import au.org.intouch.framework.interfaces.ResultCallback;
import au.org.intouch.logic.InTouchNotificationManager;
import au.org.intouch.models.ContactRequest;
import au.org.intouch.models.Notification;
import au.org.intouch.models.Relation;
import au.org.intouch.models.Role;
import au.org.intouch.models.UnassistedPerson;
import au.org.intouch.models.User;

import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * @author anthony
 *
 */
public class HomeFragment extends Fragment implements OnItemClickListener {
	protected ImageLoader imageLoader = ImageLoader.getInstance();

	private ListView list;

	public HomeFragment() {}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView   = inflater.inflate(R.layout.fragment_home, container, false);

		list = (ListView)  rootView.findViewById(R.id.list);
		list.setOnItemClickListener(this);

		updateListView();
		
		// asynchronously update the list of contact requests
		refresh();
		
		return rootView;
	}

	private boolean hasNoPrimaryCarer = false;
	private boolean hasNoContacts     = false;
	private boolean hasNoWidgets      = false;
	
	private List<RequiredAction> requiredActions = new ArrayList<RequiredAction>();
	private List<Notification>   notifications   = new ArrayList<Notification>();
	private List<ContactRequest> contactRequests = new ArrayList<ContactRequest>();

	public void showRequiredActions () {
		if (getActivity() == null) {
			return;
		}
		requiredActions.clear();
		if (hasNoContacts) {
			RequiredAction action = new RequiredAction();
			action.setMessage(getString(R.string.warning_no_contacts));
			action.setOnClickHandler(new View.OnClickListener() {
				@Override public void onClick(View view) {
					
					// go to the add contact screen
					Intent dev = new Intent(getActivity(), ContactAddActivity.class);
					startActivity(dev);
				}
			});

			requiredActions.add(action);
		} else if (hasNoPrimaryCarer) {
			RequiredAction action = new RequiredAction();
			action.setMessage(getString(R.string.warning_no_primary_carer));
			action.setOnClickHandler(new View.OnClickListener() {
				@Override public void onClick(View view) {
					
					// go to the contacts screen:
					MainActivity activity = (MainActivity) getActivity();
					activity.selectTab(MainActivity.TAB_CONTACTS);
				}
			});

			requiredActions.add(action);
		} else if (hasNoWidgets) {
			RequiredAction action = new RequiredAction();
			action.setMessage(getString(R.string.warning_no_widget));

			requiredActions.add(action);
		}
		
		
		updateListView();
	}
	
	public void updateListView () {

		final ArrayList<Object> items = new ArrayList<Object>();
		
		items.addAll(requiredActions);
		items.add(InTouch.getProvider().getLoggedInUser());
		items.addAll(contactRequests);
		items.addAll(notifications);
		
		list.setAdapter(new ObjectAdapter(getActivity(), items));
	}
	
	public void checkRequiredActions () {
		
		// find out whether or not the user has a primary carer
		if (InTouch.getLoggedInUserType().equals(UserType.AssistedPerson)) {
			InTouch.getAssistedDataProvider().getPrimaryCarer(new ResultCallback<UnassistedPerson>(){
				@Override public void done(UnassistedPerson result, InTouchException err) {
					// if there is no primary carer, then show the alert button
					hasNoPrimaryCarer = result == null;
					
					showRequiredActions();
				}
				
			});
		}
		// find out whether or not the user has any contacts
		InTouch.getProvider().getContacts(new ResultCallback<List<Relation>>() {
			@Override
			public void done(List<Relation> result, InTouchException err) {
				// if there are no contacts, then show the alert button
				hasNoContacts = result == null || result.size() == 0;
			}
		});
		
		// Check if the user has any InTouch widgets:
		int widgets[] = AssistanceAppWidgetProvider.getWidgetIds(getActivity());
		
		hasNoWidgets = widgets.length == 0;

	}
	public void refresh() {

		checkRequiredActions();
		
		// get all the user's contact requests
		InTouch.getProvider().getContactRequests(new ResultCallback<List<ContactRequest>>() {
			@Override public void done(List<ContactRequest> results, InTouchException err) {
				if(err != null) {
					// deal with it!
					err.dealWithIt(getActivity());
					// exit
					return;
				}
				
				contactRequests = results;
				
				// defensive programming
				if(getActivity() != null) {
					updateListView();
				}
			}
		});
		
		// get all the user's unread notifications
		InTouch.getProvider().getUnreadNotifications(new ResultCallback<List<Notification>>() {
			@Override
			public void done(List<Notification> result, InTouchException err) {
				if(getActivity() == null) {
					return;
				}
				
				// if there was a problem
				if(err != null) {
					// deal with it
					err.dealWithIt(getActivity());
					// exit
					return;
				}
				
				// otherwise, use the returned list of notifications
				notifications = result;
				
				updateListView();
				
				/*// try to show real notifications
				try {

					for(Notification n : result) {
						// show the notifications
						new InTouchNotificationManager(getActivity()).showNotification(n);
						// set them as read
						InTouch.getProvider().setCommunicationAsRead(n, new PassiveCallback() {
							@Override
							public void done(boolean successful,
									InTouchException err) {
								// don't care if it doesn't work
							}
						});
					}
				} catch (Exception e) {
					// ssh....
				}
				*/
			}
		});		
	}

	public String getMessage(ContactRequest cr) {
		Role _desiredRole = cr.getDesiredRole();
		User _initiator = cr.getInitiator();
		if (InTouch.getLoggedInUserType().equals(UserType.AssistedPerson)) {
			if (_desiredRole.equals(Role.Carer)) {
				return getString(R.string.request_carer, _initiator.getFullName(), _desiredRole.toString());
			} else if (_desiredRole.equals(Role.PrimaryCarer)) {
				return getString(R.string.request_primary_carer, _initiator.getFullName(), _desiredRole.toString());
			} else {
				return getString(R.string.request_friend, _initiator.getFullName(), _desiredRole.toString());
			}
		} else {
			if (_desiredRole.equals(Role.Carer)) {
				return getString(R.string.request_carer_unassisted, _initiator.getFullName(), _desiredRole.toString());
			} else if (_desiredRole.equals(Role.PrimaryCarer)) {
				return getString(R.string.request_primary_carer_unassisted, _initiator.getFullName(), _desiredRole.toString());
			} else {
				return getString(R.string.request_friend_unassisted, _initiator.getFullName(), _desiredRole.toString());
			}
		}
		
	}


	public String getDescription(ContactRequest cr) {

		Role _desiredRole = cr.getDesiredRole();
		User _initiator = cr.getInitiator();
		if (InTouch.getLoggedInUserType().equals(UserType.AssistedPerson)) {
			if (_desiredRole.equals(Role.Carer)) {
				return getString(R.string.request_description_carer, _initiator.getFullName(), _desiredRole.toString());
			} else if (_desiredRole.equals(Role.PrimaryCarer)) {
				return getString(R.string.request_description_primary_carer, _initiator.getFullName(), _desiredRole.toString());
			} else if (_desiredRole.equals(Role.Friend)) {
				return getString(R.string.request_description_friend, _initiator.getFullName(), _desiredRole.toString());
			}
		} else {
			if (_desiredRole.equals(Role.Carer)) {
				return getString(R.string.request_description_carer_unassisted, _initiator.getFullName(), _desiredRole.toString());
			} else if (_desiredRole.equals(Role.PrimaryCarer)) {
				return getString(R.string.request_description_primary_carer_unassisted, _initiator.getFullName(), _desiredRole.toString());
			} else if (_desiredRole.equals(Role.Friend)) {
				return getString(R.string.request_description_friend_unassisted, _initiator.getFullName(), _desiredRole.toString());
			}
		}
		return "";
	}
	
	/**
	 * A class to represent an error button notifying the user of something they have to do.
	 * @author anthony
	 *
	 */
	public static class RequiredAction {
		private String message;
		private OnClickListener onClickListener = null;
		
		public CharSequence getMessage() {
			if (message == null) return "";
			return message;
		}

		public void setOnClickHandler(OnClickListener value) {
			onClickListener = value;
		}

		public void setMessage(String value) {
			message = value;
		}
		
		public void onClick (View view){
			if (onClickListener != null) {
				onClickListener.onClick(view);
			}
		}
	}
	
	public void onClickContactRequest(final ContactRequest request) {
		new AlertDialog.Builder(getActivity())
	    .setTitle("Accept or Ignore?")
	    .setIcon(android.R.drawable.ic_partial_secure)
	    .setMessage(getMessage(request) + "\n\n" + getDescription(request) + "\n\nDo you accept?")
	    .setPositiveButton("Cancel",
	            new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface dialog, int id) {
	                    dialog.cancel();
	                }
	            })

	    .setNeutralButton("Deny",
	            new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface dialog, int id) {
	                	// ignore (deny) the request
	                	request.ignore();
	                	// save that to parse
	                	InTouch.getProvider().updateContactRequest(request, new PassiveCallback() {
							@Override
							public void done(boolean successful,
									InTouchException err) {
								// if there was a problem, deal with it
								if(!successful) {
									err.dealWithIt(getActivity());
								}
								// update the list of contact requests
								refresh();
							}
	                	});
	                }
	            })
	    .setNegativeButton("Accept",
	            new DialogInterface.OnClickListener() {
	                public void onClick(DialogInterface dialog, int id) {
	                	// accept the request
	                	request.accept();
	                	// save that to parse
	                	InTouch.getProvider().updateContactRequest(request, new PassiveCallback() {
						@Override
						public void done(boolean successful,
								InTouchException err) {
							// if there was a problem, deal with it
							if(!successful) {
								err.dealWithIt(getActivity());
							}
							// update the list of contact requests
							refresh();
						}
                	});
	                }
	            })
	    .show();

	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		Object item;
		Intent intent;
		
		item = list.getAdapter().getItem(position);
		
		// What happens when clicked depends on the type of object:
		if (item instanceof ContactRequest) {
			onClickContactRequest((ContactRequest) item);
		} else if (item instanceof RequiredAction) {
			((RequiredAction) item).onClick(view);
		} else if (item instanceof Notification) {
			// go to the sender's profile
			intent = new Intent(getActivity(), ProfileActivity.class);
			intent.putExtra(
				ProfileActivity.EXTRA_USER,
				((Notification) item).getSender()
			);
			startActivity(intent);
		} else if (item instanceof User){
			// go to the user's profile
			intent = new Intent(getActivity(), ProfileActivity.class);
			intent.putExtra(
				ProfileActivity.EXTRA_USER,
				(User) item
			);
			startActivity(intent);
		}
	}

	/**
	 * For showing all the new contact requests a user has received.
	 */
	private class ObjectAdapter extends ArrayAdapter<Object> {

		public ObjectAdapter(Context context, List<Object> arr) {
			super(context, R.layout.contact_list_item, arr);
		}
		public View getNotificationView(int position, View convertView, ViewGroup parent) {
			ImageView imageView;
			TextView timeView, messageView;
			Notification n;
			int color;
			Date time;
			String message;
			String imageURL;
			
			// create a new notification list item view from the layout
			convertView = LayoutInflater.from(getContext()).inflate(R.layout.notification_list_item, null);
			
			// get references to the sub views in there
			imageView = (ImageView) convertView.findViewById(R.id.image);
			timeView = (TextView) convertView.findViewById(R.id.time);
			messageView = (TextView) convertView.findViewById(R.id.message);
			
			// get the notification at this position
			n = (Notification) getItem(position);
			
			// find the color, time, message, and image url
			time = n.getTime();
			imageURL = n.getSender().getProfileImage();
			switch(n.getType()) {
				case RED:
					color = 0xfff54a4a;
					message = getString(R.string.notification_red, n.getSender().getFullName());
					break;
				case YELLOW:
					color = 0xffffdd83;
					message = getString(R.string.notification_yellow, n.getSender().getFullName());
					break;
				case GREEN:
					color = 0xffffffff;
					message = getString(R.string.notification_green, n.getSender().getFullName());
					break;
				default:
					color = Color.WHITE;
					message = "";
			}
			
			// set the background color of the whole list item
			convertView.setBackgroundColor(color);
			
			// set the time, image, and message
			timeView.setText(time.toString());
			imageLoader.displayImage(imageURL, imageView);
			messageView.setText(message);
			
			// return the generated view
			return convertView;
		}
		public View getContactRequestView(int position, View convertView, ViewGroup parent) {

			convertView = LayoutInflater.from(getContext()).inflate(R.layout.request_list_item, null);
			
			ContactRequest request = (ContactRequest) getItem(position);
			User person = request.getInitiator();
			
			TextView label  = (TextView)  convertView.findViewById(R.id.label);
			ImageView image = (ImageView) convertView.findViewById(R.id.image);
			
			label.setText(getMessage(request));
			imageLoader.displayImage(
				person.getProfileImage(),
				image
			);
			return convertView;
			
		}
		private View getMeView(int position, View convertView,
				ViewGroup parent) {

			convertView = LayoutInflater.from(getContext()).inflate(R.layout.me_item, null);

			User user = (User) getItem(position);
			
			TextView name  = (TextView) convertView.findViewById(R.id.name);
			ImageView image = (ImageView) convertView.findViewById(R.id.image);
			
			name.setText(user.getFullName());

			imageLoader.displayImage(user.getProfileImage(), image);
			
			return convertView;
		}
		private View getRequiredActionView(int position, View convertView,
				ViewGroup parent) {

			convertView = LayoutInflater.from(getContext()).inflate(R.layout.error_button_item, null);

			RequiredAction action = (RequiredAction) getItem(position);

			TextView label  = (TextView)  convertView.findViewById(R.id.label);
			label.setText(action.getMessage());
			return convertView;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {

			Object item = getItem(position);
			
			if (item instanceof ContactRequest) {
				return getContactRequestView(position, convertView, parent);
			} else if (item instanceof RequiredAction) {
				return getRequiredActionView(position, convertView, parent);
			} else if (item instanceof Notification) {
				return getNotificationView(position, convertView, parent);
			} else if (item instanceof User) {
				return getMeView(position, convertView, parent);
			}
			
			return null;

		}

		@Override
		public int getItemViewType(int position) {

			Object item = getItem(position);
			
			if (item instanceof ContactRequest) {
				return R.layout.request_list_item;
			} else if (item instanceof RequiredAction) {
				return R.layout.error_button_item;
			} else if (item instanceof Notification) {
				return R.layout.request_list_item;
			} else if (item instanceof User) {
				return R.layout.me_item;
			}
			return R.layout.error_button_item;
			
		}


	}
	
}