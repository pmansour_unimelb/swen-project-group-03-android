package au.org.intouch.activities;

import android.os.Bundle;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import au.org.intouch.InTouch;
import au.org.intouch.R;
import au.org.intouch.activities.interfaces.CleanActivity;
import au.org.intouch.logic.Device;
import au.org.intouch.logic.HelpCountdownTimer;
import au.org.intouch.logic.interfaces.DeviceFunctionality;
import au.org.intouch.models.UnassistedPerson;
import au.org.intouch.framework.exceptions.InTouchException;
import au.org.intouch.framework.interfaces.ResultCallback;

/**
 * This Activity is started when an AssistedPerson presses the help button
 * from the widget. It gives them a window of a few seconds where they
 * can cancel the help request, before it proceeds to call their primary
 * carer.
 * 
 * If onPause, onStop, or onDestroy are called, they are counted as a cancel.
 * 
 * Pre-conditions: The logged in user must be an AssistedPerson.
 */
public class HelpTimeoutActivity extends CleanActivity {
	/**
	 * [DeviceFunctionality]
	 * Optional: Default is a new Device for this Activity.
	 */
	public static final String INPUT_PHONE =
			"HelpTimeoutActivity.Phone";
	
	/**
	 * The amount of time (in milliseconds) that the user has to cancel the help
	 * request.
	 */
	private static final int CANCEL_WINDOW = 10000;
	
	
	private TextView _countdownLabel;
	private TextView _nameLabel;
	private Button _cancelButton;

	private DeviceFunctionality _phone;

	private HelpCountdownTimer _timer;
	
	public HelpTimeoutActivity() {
		// set the layout we're going to be using in this Activity
		super(R.layout.activity_help_timeout);
	}
	
	// Activity callbacks
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SharedPreferences prefs;
		
		// start the timer
		_timer = new HelpCountdownTimer(
					this,
					_countdownLabel,
					_phone,
					CANCEL_WINDOW
				);
		_timer.start();
		
		// start vibrating
		_phone.vibrate(DeviceFunctionality.VIBRATE_ALTERNATING, 0);

		// get the stored preferences for the primary carer
		prefs = getSharedPreferences(InTouch.PC_PREF, MODE_PRIVATE);
		
		// use it to set our fields that require the primary carer
		setPrimaryCarerInfo(
							prefs.getString(InTouch.PC_NAME, ""),
							prefs.getString(InTouch.PC_NUMBER, "")
						);
		
		// fetch the real current primary carer asynchronously
		InTouch.getAssistedDataProvider().getPrimaryCarer(new ResultCallback<UnassistedPerson>() {
			@Override
			public void done(UnassistedPerson result, InTouchException err) {
				// if it was successful,
				if(err == null) {
					// if there is no primary carer, exit
					if(result == null) {
						return;
					}
					// set our fields that require the primary carer
					setPrimaryCarerInfo(result.getFullName(), result.getPhoneNumber());
					// update the stored preferences
					InTouch.updatePCInfo(result);
				// otherwise,
				} else {
					// deal with it
					err.dealWithIt(HelpTimeoutActivity.this);
				}
			}
		});
	}
	@Override
	protected void onPause() {
		super.onPause();
	
		// stop vibrating
		_phone.stopVibrating();
		// stop the timer
		_timer.cancelAndExit();
	}
	
	// CleanActivity callbacks
	
	@Override
	protected void loadInputs(Intent starter) {		
		// load the phone to use
		if(starter.hasExtra(INPUT_PHONE)) {
			// if we're given one, load it
			_phone = (DeviceFunctionality)
					starter.getSerializableExtra(INPUT_PHONE);
		} else {
			// otherwise, create a new "Device" for this Activity
			_phone = new Device(this);
		}
	}
	@Override
	protected void locateViews() {
		_countdownLabel = (TextView) findViewById(R.id.activity_help_timeout_countdown);
		_nameLabel = (TextView) findViewById(R.id.activity_help_timeout_personname);
		_cancelButton = (Button) findViewById(R.id.activity_help_timeout_cancel);		
	}
	@Override
	protected void addHandlers() {
		// if the cancel button is pressed, exit the activity
		_cancelButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				_timer.cancelAndExit();
			}
		});
	}

	// other methods
	
	/**
	 * This method can be called whenever we have new information regarding
	 * the primary carer.
	 */
	private void setPrimaryCarerInfo(String name, String number) {
		// put their name on the activity layout
		_nameLabel.setText(getString(R.string.primary_carer_name, name));
		// give the number to the timer
		_timer.setPhoneNumberToCall(number);
	}
}