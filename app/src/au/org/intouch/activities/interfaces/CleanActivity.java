package au.org.intouch.activities.interfaces;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * This class defines how we should implement our Activities in a clean way:
 * 
 * 	->	Possible inputs should be defined as static final Strings at the top
 * 		of the Activity, and they should be loaded into variables in an
 * 		abstract method defined here. Optional inputs should be initialized
 * 		here as well.
 * 
 * 		These should have the prefix INPUT_xx (where xx is the input name).
 * 
 * @author peter
 *
 */
public abstract class CleanActivity extends Activity {
	
	private Integer layoutResID;
	
	/**
	 * Create a new instance of a Clean Activity with no layout.
	 */
	public CleanActivity() {
		this(null);
	}
	/**
	 * Create a new instance of a Clean Activity with a given layout.
	 * @param layoutResID The id of the layout.
	 */
	public CleanActivity(Integer layoutResID) {
		this.layoutResID = layoutResID;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// initialize the layout if there is one
		if(layoutResID != null) {
			setContentView(layoutResID);
		}
		
		// load the inputs
		loadInputs(getIntent());
		// locate the views
		locateViews();
		// initialize the values of any views
		initViewValues();
		// add any handlers if needed
		addHandlers();
	}
	
	/**
	 * This method should handle loading the inputs specified in the static
	 * variables to private instance variables.
	 */
	protected void loadInputs(Intent starter) {}
	/**
	 * This method should handle finding references to the views in the layout
	 * and saving them to private instance variables.
	 */
	protected void locateViews() {}
	/**
	 * This method should add handlers to any event in the Activity's UI that
	 * needs to be handled.
	 */
	protected void addHandlers() {}
	/**
	 * This method should set the initial values of any views in the Activity's
	 * UI.
	 */
	protected void initViewValues() {}

}
