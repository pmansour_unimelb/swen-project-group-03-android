package au.org.intouch.activities.register;

import android.os.Build;
import android.os.Bundle;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;
import android.support.v4.app.NavUtils;
import au.org.intouch.InTouch;
import au.org.intouch.R;
import au.org.intouch.activities.MainActivity;
import au.org.intouch.framework.exceptions.InTouchException;
import au.org.intouch.framework.interfaces.PassiveCallback;
import au.org.intouch.framework.interfaces.AuthenticationProvider.UserType;

public class RegistrationType extends Activity {
	/**
	 * The first name entered on RegistrationName
	 */
	public static final String EXTRA_FIRST_NAME = "RegistrationName.FirstName";
	
	/**
	 * The last name entered on RegistrationName
	 */
	public static final String EXTRA_LAST_NAME = "RegistrationName.LastName";
	
	/**
	 * The phone number entered on RegistrationPhone
	 */
	public static final String EXTRA_PHONE_NUMBER = "RegistrationPhone.PhoneNumber";
	
	/**
	 * The email entered on RegistrationEmail
	 */
	public static final String EXTRA_EMAIL = "RegistrationEmail.Email";
	
	/**
	 * The password entered on RegistrationPassword
	 */
	public static final String EXTRA_PASSWORD = "RegistrationPassword.Password";

	// views for the progress bar
	private View registerFormView;
	private View registerStatusView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration_type);
		registerFormView = findViewById(R.id.registration_type_form);
		registerStatusView = findViewById(R.id.registration_status);
		// Show the Up button in the action bar.
		setupActionBar();
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.registration_type, menu);
		return true;
	}
	
	public void confirm (View view){
		String firstName;
		String lastName;
		String phone;
		String email;
		String password;
		String type;
		UserType user;
		Intent intent;
		
		intent = getIntent();
		firstName = intent.getStringExtra(EXTRA_FIRST_NAME);
		lastName = intent.getStringExtra(EXTRA_LAST_NAME);
		phone = intent.getStringExtra(EXTRA_PHONE_NUMBER);
		email = intent.getStringExtra(EXTRA_EMAIL);
		password = intent.getStringExtra(EXTRA_PASSWORD);
		
		Spinner userType = (Spinner) findViewById(R.id.type_spinner);
		type = userType.getSelectedItem().toString();
		
		if (type.equals(getResources().getStringArray(R.array.type_choices)[0])){
			user =  UserType.UnassistedPerson;
		}
		else{
			user = UserType.AssistedPerson;
		}
		// try to register
		
		showProgress(true);

		InTouch.register(
				email,
				password,
				firstName,
				lastName,
				phone,
				user,
				new PassiveCallback() {
					@Override
					public void done(boolean successful,
							InTouchException err) {
						if(successful) {
							// hooray
							// clear the backstack and go to main
							Intent i = new Intent(RegistrationType.this, MainActivity.class);
							i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
							i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(i);
							finish();
						} else {
							// there's a problem; deal with it
							showProgress(false);
							err.dealWithIt(RegistrationType.this);
							

						}
					}
				});
		
		Toast.makeText(this, "Signing up.. Please wait.", Toast.LENGTH_LONG).show();
		
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	// close the activity when back is pressed
	public void back (View view){
		this.finish();
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			registerStatusView.setVisibility(View.VISIBLE);
			registerStatusView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							registerStatusView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			registerFormView.setVisibility(View.VISIBLE);
			registerFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							registerFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			registerStatusView.setVisibility(show ? View.VISIBLE : View.GONE);
			registerFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}
	
}
