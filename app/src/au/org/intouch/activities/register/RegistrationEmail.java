package au.org.intouch.activities.register;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import au.org.intouch.R;
import au.org.intouch.logic.Validator;

public class RegistrationEmail extends Activity {
	/**
	 * The first name entered on RegistrationName
	 */
	public static final String EXTRA_FIRST_NAME = "RegistrationName.FirstName";
	
	/**
	 * The last name entered on RegistrationName
	 */
	public static final String EXTRA_LAST_NAME = "RegistrationName.LastName";
	
	/**
	 * The phone number entered on RegistrationPhone
	 */
	public static final String EXTRA_PHONE_NUMBER = "RegistrationPhone.PhoneNumber";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration_email_and_password);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.registration_email_and_password, menu);
		return true;
	}
	
	public void next(View view){
		String firstName;
		String lastName;
		String phone;
		Intent intent;
		EditText email;
		
		// get the value of the text and put it in phone
		email = (EditText) findViewById(R.id.email);
		
		// set the values to the extras
		intent = getIntent();
		firstName = intent.getStringExtra(EXTRA_FIRST_NAME);
		lastName = intent.getStringExtra(EXTRA_LAST_NAME);
		phone = intent.getStringExtra(EXTRA_PHONE_NUMBER);
		
		// check to see if the entered email is valid
		if(checkEmail(email)){
			
			intent = new Intent(this,RegistrationPassword.class);
			intent.putExtra(RegistrationPassword.EXTRA_FIRST_NAME, firstName);
			intent.putExtra(RegistrationPassword.EXTRA_LAST_NAME, lastName);
			intent.putExtra(RegistrationPassword.EXTRA_PHONE_NUMBER, phone);
			intent.putExtra(RegistrationPassword.EXTRA_EMAIL, email.getText().toString());
			startActivity(intent);
		}
		
	}
	
	// method to check whether the email entered is valid
	private boolean checkEmail(EditText email){
		
		if(email.getText().toString().isEmpty()){
			email.setError(getString(R.string.email_empty_error));
			return false;
		}
		
		
		if(!Validator.isEmail(email.getText().toString())){
			email.setError(getString(R.string.email_error));
			return false;
		}
		return true;
	}
	
	// finish the activity when back is pressed
	public void back(View view){
		this.finish();
		return;
	}

}
