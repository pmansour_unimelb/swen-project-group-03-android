package au.org.intouch.activities.register;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.support.v4.app.NavUtils;
import au.org.intouch.R;
import au.org.intouch.logic.Validator;

public class RegistrationPassword extends Activity {
	
	/**
	 * The first name entered on RegistrationName
	 */
	public static final String EXTRA_FIRST_NAME = "RegistrationName.FirstName";
	
	/**
	 * The last name entered on RegistrationName
	 */
	public static final String EXTRA_LAST_NAME = "RegistrationName.LastName";
	
	/**
	 * The phone number entered on RegistrationPhone
	 */
	public static final String EXTRA_PHONE_NUMBER = "RegistrationPhone.PhoneNumber";
	
	/**
	 * The email entered on RegistrationEmail
	 */
	public static final String EXTRA_EMAIL = "RegistrationEmail.Email";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration_password);
		// Show the Up button in the action bar.
		setupActionBar();
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.registration_password, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void next(View view){
		String firstName;
		String lastName;
		String phone;
		String email;
		Intent intent;
		EditText password;
		EditText confirm;
		
		// get the value of the text and put it in phone
		password = (EditText) findViewById(R.id.enter_password);
		confirm = (EditText) findViewById(R.id.confirm_password);
		
		// set the values to the extras
		intent = getIntent();
		firstName = intent.getStringExtra(EXTRA_FIRST_NAME);
		lastName = intent.getStringExtra(EXTRA_LAST_NAME);
		phone = intent.getStringExtra(EXTRA_PHONE_NUMBER);
		email = intent.getStringExtra(EXTRA_EMAIL);
		
		// if the passwords are fine, go to the next activity
		if(checkPasswords(password,confirm)){
			
			intent = new Intent(this,RegistrationType.class);
			intent.putExtra(RegistrationType.EXTRA_FIRST_NAME, firstName);
			intent.putExtra(RegistrationType.EXTRA_LAST_NAME, lastName);
			intent.putExtra(RegistrationType.EXTRA_PHONE_NUMBER, phone);
			intent.putExtra(RegistrationType.EXTRA_EMAIL, email);
			intent.putExtra(RegistrationType.EXTRA_PASSWORD, password.getText().toString());
			startActivity(intent);
		}
	}
	
	// finish the activity when back is pressed
	public void back(View view){
		this.finish();
		return;
	}
	
	// method to check if password is valid
	private boolean checkPasswords(EditText password, EditText confirm){
		
		// password is empty
		if(password.getText().toString().isEmpty()){
			password.setError(getString(R.string.password_empty_error));
			
			if(confirm.getText().toString().isEmpty()){
				confirm.setError(getString(R.string.password_empty_error));
			}

			return false;
		}
		
		// passwords don't match up
		if(!password.getText().toString().equals(confirm.getText().toString())){
			password.setError(getString(R.string.password_mismatch_error));
			confirm.setError(getString(R.string.password_mismatch_error));
			return false;
		}
		
		// password is too short
		if(password.getText().toString().length() < Validator.PASSWORD_MINIMUM){
			password.setError(getString(R.string.password_error));
			confirm.setError(getString(R.string.password_error));
			return false;
		}
		
		return true;
		
	}
}
