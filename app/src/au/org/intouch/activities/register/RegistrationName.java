package au.org.intouch.activities.register;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.support.v4.app.NavUtils;
import au.org.intouch.R;

public class RegistrationName extends Activity {

	public static final String STATE_FIRST = "first_name";
	public static final String STATE_LAST = "last_name";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration_name);
		// Show the Up button in the action bar.
		setupActionBar();
		
		if(savedInstanceState != null){
			((EditText)findViewById(R.id.first_name)).setText(savedInstanceState.getString(STATE_FIRST));
			((EditText)findViewById(R.id.first_name)).setText(savedInstanceState.getString(STATE_LAST));
		}

	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.registration_name, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		
		// save the current values for the edit text in the savedInstanceState 
		savedInstanceState.putString(STATE_FIRST, ((EditText) findViewById(R.id.first_name)).getText().toString());
		savedInstanceState.putString(STATE_LAST, ((EditText) findViewById(R.id.last_name)).getText().toString());
		
		// call the superclass' method
		super.onSaveInstanceState(savedInstanceState);
	}
	
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);

		// saved state of the edit texts
		((EditText)findViewById(R.id.first_name)).setText(savedInstanceState.getString(STATE_FIRST));
		((EditText)findViewById(R.id.first_name)).setText(savedInstanceState.getString(STATE_LAST));
	}

	
	// when next is selected, check the text fields and either proceed or stop
	public void next(View view){
		EditText firstName;
		EditText lastName;
		
		// set the names to the entered text
		firstName = (EditText) findViewById(R.id.first_name);
		lastName = (EditText) findViewById(R.id.last_name);
		
		if(checkNames(firstName, lastName)){
			Intent intent = new Intent(this,RegistrationPhone.class);
			intent.putExtra(RegistrationPhone.EXTRA_FIRST_NAME, firstName.getText().toString());
			intent.putExtra(RegistrationPhone.EXTRA_LAST_NAME, lastName.getText().toString());
			startActivity(intent);
		}
	}

	// method returns true if the names are valid, false if they are not
	private boolean checkNames(EditText first, EditText last){
		boolean result = true;
		
		// first name empty error
		if(first.getText().toString().isEmpty()){
			first.setError(getString(R.string.first_name_empty_error));
			result = false;
		}
		
		// first name too short error
		else if(first.getText().toString().length() <= 1){
			first.setError(getString(R.string.first_name_error));
			result = false;
		}
		
		// last name empty error
		if(last.getText().toString().isEmpty()){
			last.setError(getString(R.string.last_name_empty_error));
			result = false;
		}
		
		// last name too short error
		else if(last.getText().toString().length() <= 1){
			last.setError(getString(R.string.last_name_error));
			result = false;
		}
		
		return result;
		
	}
	
	// close the activity when back is pressed
	public void back(View view){
		this.finish();
		return;
	}
}
