package au.org.intouch.activities.register;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import au.org.intouch.R;

public class RegistrationPhone extends Activity {
	/**
	 * The first name entered on RegistrationName
	 */
	public static final String EXTRA_FIRST_NAME = "RegistrationName.FirstName";
	
	/**
	 * The last name entered on RegistrationName
	 */
	public static final String EXTRA_LAST_NAME = "RegistrationName.LastName";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration_phone);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.registration_phone, menu);
		return true;
	}
	
	// close the activity when back is pressed
	public void back(View view) {
		this.finish();
	}
	
	public void next(View view){
		String firstName;
		String lastName;
		EditText phone;
		Intent intent;
		
		// get the value of the text and put it in phone
		phone = (EditText) findViewById(R.id.phone);
		
		intent = getIntent();
		firstName = intent.getStringExtra(EXTRA_FIRST_NAME);
		lastName = intent.getStringExtra(EXTRA_LAST_NAME);
		
		if (checkPhoneNumber(phone)){
			// pass the values down to the next activity
			intent = new Intent(this, RegistrationEmail.class);
			intent.putExtra(RegistrationEmail.EXTRA_FIRST_NAME, firstName);
			intent.putExtra(RegistrationEmail.EXTRA_LAST_NAME, lastName);
			intent.putExtra(RegistrationEmail.EXTRA_PHONE_NUMBER, phone.getText().toString());
			
			// start the next activity
			startActivity(intent);
		}
	}
	
	// method to check if the phone number is valid
	// returns true if it is
	private boolean checkPhoneNumber(EditText number){
		
		// phone number empty error
		if(number.getText().toString().isEmpty()){
			number.setError(getString(R.string.phone_empty_error));
			return false;
		}
		
		// this is where the regular expression should go
		if(number.getText().toString().length() < 5 || number.getText().toString().length() > 20){
			number.setError(getString(R.string.phone_error));
			return false;
		}
		
		return true;
	}

}
