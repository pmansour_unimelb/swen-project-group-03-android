package au.org.intouch.activities;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import au.org.intouch.InTouch;
import au.org.intouch.R;
import au.org.intouch.framework.exceptions.InTouchException;
import au.org.intouch.framework.interfaces.PassiveCallback;
import au.org.intouch.models.Role;

public class ContactAddActivity extends Activity {
	public static final int TOAST_DURATION = Toast.LENGTH_LONG;

	EditText emailView;
	Spinner desiredRoleView;
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		return true;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact_add);

		if (savedInstanceState == null) {

		}
		
		emailView = (EditText) findViewById(R.id.email);
		desiredRoleView = (Spinner) findViewById(R.id.desired_role);
		
		emailView
		.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
				if (id == R.id.add || id == EditorInfo.IME_NULL) {
					addContact(emailView.getText().toString(), desiredRoleView.getSelectedItem().toString());
					return true;
				}
				return false;
			}
		});
		

		findViewById(R.id.addButton).setOnClickListener(
			new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					addContact(emailView.getText().toString(), desiredRoleView.getSelectedItem().toString());
				}
			}
		);
	}
	
	private void addContact(final String email, String strDesiredRole) {
		Role desiredRole;
		
		desiredRole = Role.fromString(strDesiredRole);
		
		// perform the contact request with the following callback
		InTouch.getProvider().sendContactRequest(email, desiredRole, new PassiveCallback() {
			@Override
			public void done(boolean successful, InTouchException err) {
				String toastMessage;
				
				// choose the right message to show on a toast
				if(successful) {
					toastMessage = String.format(getString(R.string.contact_request_success), email);
				} else {
					toastMessage = String.format(getString(R.string.contact_request_failure), email);
				}
				
				// show the toast
				Toast.makeText(getApplicationContext(), toastMessage, TOAST_DURATION).show();
			}
		});

		finish();
	}

}
