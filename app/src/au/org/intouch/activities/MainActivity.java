package au.org.intouch.activities;

import java.util.Locale;

import com.parse.ParseAnalytics;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import au.org.intouch.InTouch;
import au.org.intouch.R;
import au.org.intouch.activities.fragments.ContactListFragment;
import au.org.intouch.activities.fragments.ConversationListFragment;
import au.org.intouch.activities.fragments.HomeFragment;
import au.org.intouch.framework.exceptions.InTouchException;
import au.org.intouch.framework.interfaces.PassiveCallback;
import au.org.intouch.models.User;

public class MainActivity extends FragmentActivity implements
		ActionBar.TabListener {

	// Tab constants for selectTab
	public static final int TAB_ACTIVITY = 0;
	public static final int TAB_MESSAGES = 1;
	public static final int TAB_CONTACTS = 2;

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		/*// DEBUG - REMOVE THIS LATER
		if(InTouch.isLoggedIn()) {
			au.org.intouch.models.AssistedPerson sender = new au.org.intouch.models.AssistedPerson("person1");
			sender.setFirstName("Ivan");
			sender.setLastName("Markovich");
			sender.setEmail("imarko@vich.com");
			sender.setPhoneNumber("0425549000");
			au.org.intouch.models.Notification red = new au.org.intouch.models.Notification("red1");
			red.setRead(false);
			red.setSender(sender);
			red.setRecipient(InTouch.getProvider().getLoggedInUser());
			red.setTime(new java.util.Date());
			red.setType(au.org.intouch.models.Notification.NotificationType.RED);
			new au.org.intouch.logic.InTouchNotificationManager(this).showNotification(red);
		}*/
		
        // To track statistics around application opens
        ParseAnalytics.trackAppOpened(getIntent());
		
        // if the user is not logged in, go to the login activity
        if(InTouch.isLoggedIn() == false) {
        	Intent login = new Intent(this, LoginActivity.class);
        	login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
        	login.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        	startActivity(login);
        	finish();
        }
		
        
        // Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
			case R.id.myProfile:
			
				Intent intent = new Intent(this, ProfileActivity.class);
				User me = InTouch.getProvider().getLoggedInUser();
				intent.putExtra(
					ProfileActivity.EXTRA_USER,
					me
				);
				
				startActivity(intent);
			
				return true;
			case R.id.addContact:
				
				Intent dev = new Intent(this, ContactAddActivity.class);
				startActivity(dev);
				
				return true;
			case R.id.logout:

				InTouch.logout(new PassiveCallback() {

					@Override
					public void done(boolean successful, InTouchException err) {
						if(successful) {
							// clear the backstack and logout
							Intent login = new Intent(MainActivity.this, LoginActivity.class);
							login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 
							login.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
							startActivity(login);
				        	finish();
							
						} else {
							// deal with it
							err.dealWithIt(MainActivity.this);
						}
					}
					
				});
				
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab,
			FragmentTransaction fragmentTransaction) {
	}
	
	/**
	 * Use MainActivity.TAB_*
	 * @param tabConstant
	 */
	public void selectTab(int tabConstant) {
		ActionBar ab = getActionBar();
		
		ab.setSelectedNavigationItem(tabConstant);
	}
	
	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			if (position == 0) {
				Fragment fragment = new HomeFragment();
				return fragment;
			} else if (position == 1) {
				Fragment fragment = new ConversationListFragment();
				return fragment;
			} else {
				Fragment fragment = new ContactListFragment();
				return fragment;
			}
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_home).toUpperCase(l);
			case 1:
				return getString(R.string.title_messages).toUpperCase(l);
			case 2:
				return getString(R.string.title_contacts).toUpperCase(l);
			}
			return null;
		}
	}
}
