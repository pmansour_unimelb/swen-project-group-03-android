package au.org.intouch.activities;

import java.util.Locale;

import au.org.intouch.InTouch;
import au.org.intouch.R;
import au.org.intouch.activities.fragments.ConversationFragment;
import au.org.intouch.activities.fragments.ProfileStatusFragment;
import au.org.intouch.models.User;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

/**
 * The activity which manages tabs ("Status" and "Messages")
 * 
 * It would be nice if this was a bit thinner, but this was the default created by the android SDK,
 * and claims it "will keep every loaded fragment in memory" (sounds good), but I wouldn't know to
 * ensure that otherwise.
 * 
 * @author anthony
 *
 */
public class ProfileActivity extends FragmentActivity implements
		ActionBar.TabListener {
	public static final String EXTRA_USER = "ProfileActivity.User";
	public static final String EXTRA_TAB = "tab";
	public static final int TAB_MESSAGES = 1;
	public static final int TAB_STATUS = 0;

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	private ViewPager mViewPager;
	private User user;

	/**
	 * Is the user viewing their own profile. If so, some things are disabled / hidden.
	 * @return
	 */
	public boolean isViewingMyself() {
		return InTouch.getProvider().getLoggedInUser().getId().equals(user.getId());
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);

		user = (User) getIntent().getSerializableExtra(EXTRA_USER);
		final ActionBar actionBar = getActionBar();
		actionBar.setTitle(user.getFullName());
		

		if (!isViewingMyself()) {
			// only show tabs when not viewing your own profile (since there is no Messages tab in that case)
			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		}
		

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(
			getSupportFragmentManager()
		);

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager
		.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}
		});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab()
			.setText(mSectionsPagerAdapter.getPageTitle(i))
			.setTabListener(this));
		}
		int tabId = getIntent().getIntExtra(EXTRA_TAB, -1);
		
		if (tabId != -1) {
			selectTab(tabId);
		}
	}

	public void selectTab(int tabId) {
		// if viewing your own profile, there are no tabs
		if (isViewingMyself()) {
			return;
		}

		ActionBar ab = getActionBar();
		
		ab.setSelectedNavigationItem(tabId);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
	
		// if a user is viewing themselves, the menu does not make sense because all it has is remove
		if (isViewingMyself()) return true;
		
		getMenuInflater().inflate(R.menu.profile, menu);
		return true;
	}
	
	public void removeContact() {
		new AlertDialog.Builder(this)
		.setIcon(android.R.drawable.ic_delete)
		.setTitle("Remove")
		.setMessage("Are you sure you want to remove this person from your contact list?")
		.setNegativeButton(android.R.string.yes, new DialogInterface.OnClickListener() {
			@Override public void onClick(DialogInterface dialog, int which) {
				ProfileActivity.this.finish();	
			}
		})
		.setPositiveButton(android.R.string.no, null)
		.show();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.remove:
				removeContact();
				return true;

			default:
				return super.onOptionsItemSelected(item);
		}
	}
	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	}
	
	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			
			Fragment fragment = null;

			if (position == 0) {
				fragment = new ProfileStatusFragment();
			} else if (position == 1) {
				fragment = new ConversationFragment();
			}

			return fragment;
		}

		@Override public int getCount() {
			if (isViewingMyself()) {
				// hide the messages tab
				return 1;
			}
			return 2;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
				case 0: return getString(R.string.title_status).toUpperCase(l);
				case 1: return getString(R.string.title_messages).toUpperCase(l);
			}
			return null;
		}
	}


}
