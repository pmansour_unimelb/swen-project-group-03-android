package au.org.intouch.models;

import java.io.Serializable;

/**
 * The superclass of all the entity models. Just includes an id that's set
 * in the constructor, and a getter for that id.
 * @author peter
 *
 */
public abstract class Entity implements Serializable {
	private static final long serialVersionUID = -7349513130899716189L;
	
	private String _id;
	
	public Entity(String id) {
		_id = id;
	}
	
	public String getId() {
		return _id;
	}
}
