package au.org.intouch.models;

public enum Role {

	PrimaryCarer("Primary Carer"),
	Carer("Carer"),
	Friend("Friend");
	
	private String _str;
	
	Role(String str) {
		_str = str;
	}
	
	@Override
	public String toString() {
		return _str;
	}
	
	/**
	 * Complementary to toString()
	 */
	public static Role fromString(String str) {
		if(PrimaryCarer.toString().equals(str)) {
			return PrimaryCarer;
		} else if(Carer.toString().equals(str)) {
			return Carer;
		} else if(Friend.toString().equals(str)) {
			return Friend;
		} else {
			return null;
		}
	}
}
