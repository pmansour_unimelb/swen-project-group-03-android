package au.org.intouch.models;

import java.util.Date;

/**
 * Represents a user of our system who is an assisted person.
 * 
 * This class offers an interface that allows all outside dealings
 * to be in terms of actual entity instances, while in reality the
 * references are mostly stored as string UIDs.
 * 
 * @author peter
 *
 */
public class AssistedPerson extends User {
	private static final long serialVersionUID = -2240376111899359376L;
	
	// private instance variables
	
	private Date _lastCommunicationTime;
	private Date _lastInteractionTime;
	
	// constructor
	
	public AssistedPerson(String id) {
		super(id);
	}
	
	// getters and setters
	
	public Date getLastCommunicationTime() {
		return _lastCommunicationTime;
	}
	public void setLastCommunicationTime(Date value) {
		_lastCommunicationTime = value;
	}

	public Date getLastInteractionTime() {
		return _lastInteractionTime;
	}
	public void setLastInteractionTime(Date value) {
		_lastInteractionTime = value;
	}
	
	// helpful methods

	/**
	 * Two AssistedPersons are equal if their superclass are equal, they're
	 * both AssistedPersons, and they have the same values for the fields.
	 */
	@Override
	public boolean equals(Object o) {
		AssistedPerson ap;
		
		// make sure that their superclasses are equal
		if(super.equals(o) == false) {
			return false;
		}
		
		// make sure that o is an AssistedPerson
		if((o instanceof AssistedPerson) == false) {
			return false;
		}
		
		// cast o into an AssistedPerson to check its fields
		ap = (AssistedPerson) o;
		
		// make sure they have the same fields
		if(
				_lastCommunicationTime.equals(ap._lastCommunicationTime) == false ||
				_lastInteractionTime.equals(ap._lastInteractionTime) == false
			) {
			return false;
		}
		
		// since o passed all the tests, then it's equal
		return true;
	}

}
