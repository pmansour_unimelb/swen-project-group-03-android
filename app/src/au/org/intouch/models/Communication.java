package au.org.intouch.models;

import java.util.Date;

public abstract class Communication extends Entity {
	private static final long serialVersionUID = -6426952148227097748L;
	
	// private instance variables
	
	private User _sender;
	private User _recipient;
	private Date _time;
	private boolean _read;
	
	// constructor
	
	public Communication(String id) {
		super(id);
	}
	
	// getters and setters
	
	public User getSender() {
		return _sender;
	}
	public void setSender(User value) {
		_sender = value;
	}
	
	public User getRecipient() {
		return _recipient;
	}
	public void setRecipient(User value) {
		_recipient = value;
	}
	
	public Date getTime() {
		return _time;
	}
	public void setTime(Date value) {
		_time = value;
	}
	
	public boolean getRead() {
		return _read;
	}
	public void setRead(boolean value) {
		_read = value;
	}

	// helpful methods

	/**
	 * Two Communications are equal if they are both Communications and their
	 * fields are equal.
	 * Id is taken into account.
	 */
	public boolean equals(Object o) {
		Communication c;
		
		// make sure o is not null
		if(o == null) {
			return false;
		}
		
		// make sure o is a Communication
		if((o instanceof Communication) == false) {
			return false;
		}
		
		// cast o into a Communication to check the rest of the fields
		c = (Communication) o;
		
		// make sure they have the same id
		if(getId().equals(c.getId()) == false) {
			return false;
		}
		
		// make sure the fields are equal
		if(
				_sender.equals(c._sender) == false ||
				_recipient.equals(c._recipient) == false ||
				_time.equals(c._time)== false ||
				_read != c._read
			) {
			return false;
		}
		
		// since o passed all the tests, it's equal
		return true;
	}
	
}
