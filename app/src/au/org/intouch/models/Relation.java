package au.org.intouch.models;

public class Relation extends Entity {
	private static final long serialVersionUID = 1209176180064675L;
	
	// private instance variables
	
	private User _ap;
	private User _up;
	private Role _role;
	
	// constructor
	
	public Relation(String id) {
		super(id);
	}
	
	// getters and setters
	
	public User getAssistedPerson() {
		return _ap;
	}
	public void setAssistedPerson(User value) {
		_ap = value;
	}
	
	public User getUnassistedPerson() {
		return _up;
	}
	public void setUnassistedPerson(User value) {
		_up = value;
	}
	
	public Role getRole() {
		return _role;
	}
	public void setRole(Role value) {
		_role = value;
	}

	// helper methods
	
	/**
	 * Two relations are equal if they're both relations and they have the same
	 * ap, up, and role.
	 * Id is taken into account as well.
	 */
	@Override
	public boolean equals(Object o) {
		Relation r;
		
		// make sure the given object isn't null
		if(o == null) {
			return false;
		}
		
		// make sure the given object is a relation
		if(!(o instanceof Relation)) {
			return false;
		}
		
		// cast it to a relation to do the rest of the checks
		r = (Relation) o;
		
		// make sure they have the same id
		if(getId().equals(r.getId()) == false) {
			return false;
		}
		
		// make sure they have the same fields
		if(
				_ap.equals(r._ap) == false ||
				_up.equals(r._up) == false ||
				_role.equals(r._role) == false
			) {
			return false;
		}
		
		// since r passed all the tests, they're equal
		return true;
	}
}
