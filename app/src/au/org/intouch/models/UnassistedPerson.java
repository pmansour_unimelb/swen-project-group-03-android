package au.org.intouch.models;

/**
 * Represents a user of our system who is not an assisted person.
 * @author peter
 *
 */
public class UnassistedPerson extends User {
	private static final long serialVersionUID = -8273282826009191874L;
	
	// constructor

	public UnassistedPerson(String id) {
		super(id);
	}
	
	// helpful methods
	
	/**
	 * Two UnassistedPersons are equal if their superclasses are equal and
	 * they are both UnassistedPersons.
	 */
	@Override
	public boolean equals(Object o) {
		// make sure their superclasses are equal
		if(super.equals(o) == false) {
			return false;
		}
		
		// make sure that o is an UnassistedPerson
		if((o instanceof UnassistedPerson) == false) {
			return false;
		}
		
		// since o passed all the tests, then it's equal
		return true;
	}

}
