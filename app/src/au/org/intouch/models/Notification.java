package au.org.intouch.models;

public class Notification extends Communication {
	private static final long serialVersionUID = 1814213411212363319L;

	public static enum NotificationType { GREEN, YELLOW, RED };

	// private instance variables
	
	NotificationType _type;
	
	// constructor
	
	public Notification(String id) {
		super(id);
	}
	
	// getters and setters
	
	public NotificationType getType() {
		return _type;
	}
	public void setType(NotificationType value) {
		_type = value;
	}
	
	// helpful methods
	
	/**
	 * Two Notifications are equal if their superclasses are equal, they're
	 * both Notifications, and their fields are equal.
	 */
	public boolean equals(Object o) {
		Notification n;
		
		// make sure their superclasses are equal
		if(super.equals(o) == false) {
			return false;
		}
		
		// make sure o is a Notification
		if((o instanceof Notification) == false) {
			return false;
		}
		
		// cast o into a Notification to check the rest of its fields
		n = (Notification) o;
		
		// make sure the fields are equal
		if(
				_type.equals(n._type) == false
			) {
			return false;
		}
		
		// since o passed all the tests, it's equal
		return true;
	}

}
