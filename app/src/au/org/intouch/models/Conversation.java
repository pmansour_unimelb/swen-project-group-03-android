package au.org.intouch.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Conversation {
	
	private User with;
	
	private ArrayList<Communication> communications;
	
	public Conversation(User with) {
		this.with = with;
		this.communications = new ArrayList<Communication>();
	}
	
	public User getUser() {
		return with;
	}
	
	public void addCommunication(Communication comm) {
		communications.add(comm);
	}
	
	public ArrayList<Communication> getCommunications() {
		return communications;
	}
	
	public static List<Conversation> collate(User loggedInUser, List<Communication> communications) {
		HashMap<String, Conversation> map;
		User withWho;
		
		// index conversations by the id of the person they're with
		map = new HashMap<String, Conversation>(communications.size());

		// go through all the communications
		for(Communication comm : communications) {
			// try to find the person they're with
			// (so if the logged in user is the sender, then its the recipient,
			// and vice versa)
			if(comm.getSender().getId().equals(loggedInUser.getId())) {
				withWho = comm.getRecipient();
			} else {
				withWho = comm.getSender();
			}
			
			// add a new conversation to the map if needed
			if(map.containsKey(withWho.getId()) == false) {
				map.put(withWho.getId(), new Conversation(withWho));
			}
			
			// add the actual communication to that conversation
			map.get(withWho.getId()).addCommunication(comm);
		}
		
		// return the conversations generated
		return new ArrayList<Conversation>(map.values());
	}
	
}
