package au.org.intouch.models;

public abstract class User extends Entity {
	private static final long serialVersionUID = -5738654256077673082L;
	
	// private instance variables
	
	private String firstName;
	private String lastName;
	private String email;
	private String phoneNumber;
	// TODO: remove this
	private String profileImage = "https://s3-ap-southeast-2.amazonaws.com/intouch-app/test/matrix.png";
	
	// constructor

	public User(String id) {
		super(id);
	}
	
	// getters and setters
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String value) {
		firstName = value;
	}
	
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String value) {
		lastName = value;
	}
	
	public String getFullName() {
		return firstName + ' ' + lastName;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String value) {
		email = value;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String value) {
		phoneNumber = value;
	}
	
	public String getProfileImage() {
		return profileImage;
	}
	// WARNING: this method doesn't actually save that url to the saved user entity
	public void setProfileImage(String url) {
		profileImage = url;
	}
	
	// helper methods
	
	/**
	 * Two users are equal if they're both users and have the same fields.
	 * Id is taken into account.
	 */
	@Override
	public boolean equals(Object o) {
		User u;
		
		// make sure the given object's not null
		if(o == null) {
			return false;
		}
		
		// make sure the given object's a User
		if(!(o instanceof User)) {
			return false;
		}
		
		// cast it to a user to do the rest of the checks
		u = (User) o;
		
		// make sure the id is the same
		if(getId().equals(u.getId()) == false) {
			return false;
		}
		
		// make sure the rest of the fields are the same
		if(
				firstName.equals(u.firstName) == false ||
				lastName.equals(u.lastName) == false ||
				email.equals(u.email) == false ||
				phoneNumber.equals(u.phoneNumber) == false ||
				profileImage.equals(u.profileImage) == false
			) {
			return false;
		}
		
		// since o passed all the tests, it's equal
		return true;
	}

}
