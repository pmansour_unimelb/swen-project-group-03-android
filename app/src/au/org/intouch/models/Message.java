package au.org.intouch.models;

public class Message extends Communication {
	private static final long serialVersionUID = 7136460334018581153L;
	
	// private instance variables
	
	private String _body;

	// constructor
	
	public Message(String id) {
		super(id);
	}
	
	// getters and setters
	
	public String getBody() {
		return _body;
	}
	public void setBody(String value) {
		_body = value;
	}
	
	// helpful methods

	/**
	 * Two Messages are equal if their superclasses are equal, they're both
	 * Messages, and their fields are equal.
	 */
	public boolean equals(Object o) {
		Message m;
		
		// make sure the superclasses are equal
		if(super.equals(o) == false) {
			return false;
		}
		
		// make sure o is a Message
		if((o instanceof Message) == false) {
			return false;
		}
		
		// cast o into a Message to check the rest of its fields
		m = (Message) o;
		
		// make sure the fields are equal
		if(
				_body.equals(m._body) == false
			) {
			return false;
		}
		
		// since o passed all the tests, it's equal
		return true;
	}

}
