package au.org.intouch.models;

/**
 * Contact requests can be initiated by a user towards another user.
 * They are implicitly pending, and when they are accepted/denied their
 * object will be deleted.
 * 
 * Assistance Offer		= UnassistedPerson	-> AssistedPerson
 * Assistance Request	= AssistedPerson	-> UnassistedPerson
 * 
 * @author peter
 *
 */
public class ContactRequest extends Entity {
	private static final long serialVersionUID = -1720169889096673809L;
	
	// private instance variables
	
	private User _initiator;
	private String _recipientEmail;
	
	/** The role that the assisted person would like the unassisted person to take. */
	private Role _desiredRole;
	
	/** Whether the recipient has responded to the request (Yes or No) */
	private boolean responded = false;
	
	/** Has the recipient accepted the request yet */
	private boolean accepted = false;
	
	// constructor
	
	public ContactRequest(String id) {
		super(id);
	}


	public void ignore() {
		responded = true;
		accepted = false;
	}
	
	public void accept() {
		responded = true;
		accepted = true;
	}
	
	// getters and setters
	
	public User getInitiator() {
		return _initiator;
	}
	public void setInitiator(User value) {
		_initiator = value;
	}
	
	public String getRecipientEmail() {
		return _recipientEmail;
	}
	public void setRecipientEmail(String value) {
		_recipientEmail = value;
	}
	
	public Role getDesiredRole() {
		return _desiredRole;
	}
	public void setDesiredRole(Role value) {
		_desiredRole = value;
	}
	
	// helpful methods
	
	/**
	 * Two ContactRequests are equal if they are both ContactRequests and their
	 * fields are equal.
	 * Id is taken into account.
	 */
	public boolean equals(Object o) {
		ContactRequest cr;
		
		// make sure o is not null
		if(o == null) {
			return false;
		}
		
		// make sure o is a ContactRequest
		if((o instanceof ContactRequest) == false) {
			return false;
		}
		
		// cast o into a ContactRequest to check the rest of its fields
		cr = (ContactRequest) o;
		
		// make sure they have the same id
		if(getId().equals(cr.getId()) == false) {
			return false;
		}
		
		// make sure the fields are equal
		if(
				_initiator.equals(cr._initiator) == false ||
				_recipientEmail.equals(cr._recipientEmail) == false ||
				_desiredRole.equals(cr._desiredRole) == false
			) {
			return false;
		}
		
		// since o passed all the tests, it's equal
		return true;
	}


	public Object getAccepted() {
		return accepted;
	}
	public void setAccepted(boolean value) {
		accepted = value;
	}

	public Object getResponded() {
		return responded;
	}
	public void setResponded(boolean value) {
		responded = value;
	}


}
